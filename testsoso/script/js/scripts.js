function check_form()
    {
	var elocation  = $.trim($('#elocation').val());
	var ename= $.trim($('#ename').val());
	var etime = $.trim($('#etime').val());
	var ediscribe = $.trim($('#ediscribe').val());
	var eperson = $.trim($('#eperson').val());
	var ecate = $.trim($('#ecate').val());
//任意参数不存在return false

        if(!etime)
        {
            alert('时间为空，请重新填写~！');
            return false;
        }
		if(ecate!=="发文"){
			if(!ename){
				alert('事件名称为空，请重新填写！');
				return false;
			}
		}
	
           var form_data = $('#form_data').serialize();
        // 异步提交数据到action/add_action.php页面
        $.ajax(
                {
                    url: "/OpaEventInsert",
                    data:{"form_data":form_data,"ename":ename,"etime":etime,"ediscribe":ediscribe,"elocation":elocation,"eperson":eperson,"ecate":ecate},
                    type: "post",
		    beforeSend:function()
                    {
                      // $("#tip").html("<span style='color:blue'>正在处理...</span>");
                       // return true;
                    },
                    success:function(data)
                    {
		        if(data > 0)
                        {

						//alert("操作成功!");
						window.location.href = "/opaevents";                        

						}
                        else
                        {
                            alert('操作失败');
                        }
                    }, 
                    error:function()
                    {
                        alert('请求出错');
                    },
                    complete:function()
                    {
                        //$('#acting_tips').hide();
                    }
                });

        return false;
    }




function check_form_teamcenter()
    {
	var starttime  = $.trim($('#starttime').val());
	var stoptime= $.trim($('#stoptime').val());
	var teamid = $.trim($('#teamid').val());
//任意参数不存在return false

        if(!starttime || !stoptime )
        {
            alert('某项参数为空!!!请重新填写~！');
            return false;
        }
        return true;
    }


	
$('#ecate').change(function(){
	var ecate = $('#ecate').val();
	if(ecate=="发文"){
		$('#discribe').hide();
		$('#location').hide();
		$('#enamediv').hide();
		$("#epersondiv label").html("发文人");

	}else{
		$('#discribe').show();
		$('#location').show();
		$('#enamediv').show();
		if(ecate == "粉丝答疑" ){
			$("#enamediv label").html("答疑题目");
			$("#epersondiv label").html("答疑人");
			$("#discribe label").html("答疑描述");
			$("#location label").html("答疑地点");
			
		}
		if(ecate == "外部答疑" ){
			$("#enamediv label").html("答疑题目");
			$("#epersondiv label").html("答疑人");
			$("#discribe label").html("答疑描述");
			$("#location label").html("答疑地点");
		}
		if(ecate == "外部级分享" ){
			$("#enamediv label").html("分享主题");
			$("#epersondiv label").html("分享人");
			$("#discribe label").html("分享描述");
			$("#location label").html("分享地点");
			
		}
		if(ecate == "公司级分享" ){
			$("#enamediv label").html("分享主题");
			$("#epersondiv label").html("分享人");
			$("#discribe label").html("分享描述");
			$("#location label").html("分享地点");
		}
	}
	
	
	
	
});

