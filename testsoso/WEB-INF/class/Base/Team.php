<?php
/**
 * @author moonzhang (moonzhang@sogou-inc.com)
 * @version v1.0 2017-04-19 19:01:12 PM
 * Base table class : Team - (utf8)
 */

class Base_Team extends SOSO_ORM_Table/*Object*/ {
	/**
	 * 
	 * Class Member Mapping to Table "Team" Field "TeamId"
	 * Refer to $this->mMapHash['TeamId'];
	 * @access public
	 * @var int(11)
	*/
	public $mTeamId;

	/**
	 * 团队表格
	 * Class Member Mapping to Table "Team" Field "TeamName"
	 * Refer to $this->mMapHash['TeamName'];
	 * @access public
	 * @var varchar(255)
	*/
	public $mTeamName;

	
	public function __construct($pTable="Team",$pIndex="0") {
		parent::__construct($pTable,$pIndex);
	}
	
	public function prepareHashMap(){
		$tFields = array (
		  'Fields' => 
		  array (
		    'TeamId' => 
		    array (
		      'Type' => 'int(11)',
		      'Null' => 'NO',
		      'Key' => 'PRI',
		      'Default' => NULL,
		    ),
		    'TeamName' => 
		    array (
		      'Type' => 'varchar(255)',
		      'Null' => 'NO',
		      'Key' => '',
		      'Default' => NULL,
		    ),
		  ),
		  'Primary' => 
		  array (
		    0 => 'TeamId',
		  ),
		  'auto' => 'TeamId',
		  'charset' => 'utf8',
		);
		
		$this->tableFieldHash = array();
		$this->primaryKey = $tFields['Primary'];
		$this->autoKey = $tFields['auto'];
		$this->charset = $tFields['charset'];
		$columns = new ArrayObject(array_keys($tFields['Fields']));
		foreach ($columns as $k=>$v){
			$key = $this->genKey($v);
			$this->{$key} = &$this->hashMap[$v];
			$field = $this->criteria->isIgnoreCase() ? strtolower($v) : $v;
			$this->tableFieldHash[$field] = array('Column'=>$v)+$tFields['Fields'][$v];
		}
		$this->mSQLCommand->setCharset($this->charset);
	}
}
?>