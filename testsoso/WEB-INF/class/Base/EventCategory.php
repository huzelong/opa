<?php
/**
 * @author moonzhang (moonzhang@sogou-inc.com)
 * @version v1.0 2017-04-19 19:01:12 PM
 * Base table class : EventCategory - (utf8)
 */

class Base_EventCategory extends SOSO_ORM_Table/*Object*/ {
	/**
	 * 
	 * Class Member Mapping to Table "EventCategory" Field "EventId"
	 * Refer to $this->mMapHash['EventId'];
	 * @access public
	 * @var int(11)
	*/
	public $mEventId;

	/**
	 * 
	 * Class Member Mapping to Table "EventCategory" Field "EventCategory"
	 * Refer to $this->mMapHash['EventCategory'];
	 * @access public
	 * @var varchar(255)
	*/
	public $mEventCategory;

	
	public function __construct($pTable="EventCategory",$pIndex="0") {
		parent::__construct($pTable,$pIndex);
	}
	
	public function prepareHashMap(){
		$tFields = array (
		  'Fields' => 
		  array (
		    'EventId' => 
		    array (
		      'Type' => 'int(11)',
		      'Null' => 'NO',
		      'Key' => 'PRI',
		      'Default' => NULL,
		    ),
		    'EventCategory' => 
		    array (
		      'Type' => 'varchar(255)',
		      'Null' => 'YES',
		      'Key' => '',
		      'Default' => NULL,
		    ),
		  ),
		  'Primary' => 
		  array (
		    0 => 'EventId',
		  ),
		  'auto' => 'EventId',
		  'charset' => 'utf8',
		);
		
		$this->tableFieldHash = array();
		$this->primaryKey = $tFields['Primary'];
		$this->autoKey = $tFields['auto'];
		$this->charset = $tFields['charset'];
		$columns = new ArrayObject(array_keys($tFields['Fields']));
		foreach ($columns as $k=>$v){
			$key = $this->genKey($v);
			$this->{$key} = &$this->hashMap[$v];
			$field = $this->criteria->isIgnoreCase() ? strtolower($v) : $v;
			$this->tableFieldHash[$field] = array('Column'=>$v)+$tFields['Fields'][$v];
		}
		$this->mSQLCommand->setCharset($this->charset);
	}
}
?>