<?php
/**
 * @author moonzhang (moonzhang@sogou-inc.com)
 * @version v1.0 2017-04-19 19:01:12 PM
 * Base table class : Article - (utf8)
 */

class Base_Article extends SOSO_ORM_Table/*Object*/ {
	/**
	 * 
	 * Class Member Mapping to Table "Article" Field "NetName"
	 * Refer to $this->mMapHash['NetName'];
	 * @access public
	 * @var varchar(255)
	*/
	public $mNetName;

	/**
	 * 
	 * Class Member Mapping to Table "Article" Field "ArticleTitle"
	 * Refer to $this->mMapHash['ArticleTitle'];
	 * @access public
	 * @var varchar(255)
	*/
	public $mArticleTitle;

	/**
	 * 
	 * Class Member Mapping to Table "Article" Field "PageViews"
	 * Refer to $this->mMapHash['PageViews'];
	 * @access public
	 * @var int(255)
	*/
	public $mPageViews;

	/**
	 * 
	 * Class Member Mapping to Table "Article" Field "Reprint"
	 * Refer to $this->mMapHash['Reprint'];
	 * @access public
	 * @var int(255)
	*/
	public $mReprint;

	/**
	 * 
	 * Class Member Mapping to Table "Article" Field "Share"
	 * Refer to $this->mMapHash['Share'];
	 * @access public
	 * @var int(255)
	*/
	public $mShare;

	/**
	 * 
	 * Class Member Mapping to Table "Article" Field "Fav"
	 * Refer to $this->mMapHash['Fav'];
	 * @access public
	 * @var int(255)
	*/
	public $mFav;

	/**
	 * 
	 * Class Member Mapping to Table "Article" Field "Message"
	 * Refer to $this->mMapHash['Message'];
	 * @access public
	 * @var int(255)
	*/
	public $mMessage;

	/**
	 * 
	 * Class Member Mapping to Table "Article" Field "LauchDate"
	 * Refer to $this->mMapHash['LauchDate'];
	 * @access public
	 * @var date
	*/
	public $mLauchDate;

	/**
	 * 
	 * Class Member Mapping to Table "Article" Field "StatisticDate"
	 * Refer to $this->mMapHash['StatisticDate'];
	 * @access public
	 * @var date
	*/
	public $mStatisticDate;

	
	public function __construct($pTable="Article",$pIndex="0") {
		parent::__construct($pTable,$pIndex);
	}
	
	public function prepareHashMap(){
		$tFields = array (
		  'Fields' => 
		  array (
		    'NetName' => 
		    array (
		      'Type' => 'varchar(255)',
		      'Null' => 'YES',
		      'Key' => 'MUL',
		      'Default' => NULL,
		    ),
		    'ArticleTitle' => 
		    array (
		      'Type' => 'varchar(255)',
		      'Null' => 'YES',
		      'Key' => '',
		      'Default' => NULL,
		    ),
		    'PageViews' => 
		    array (
		      'Type' => 'int(255)',
		      'Null' => 'YES',
		      'Key' => '',
		      'Default' => NULL,
		    ),
		    'Reprint' => 
		    array (
		      'Type' => 'int(255)',
		      'Null' => 'YES',
		      'Key' => '',
		      'Default' => NULL,
		    ),
		    'Share' => 
		    array (
		      'Type' => 'int(255)',
		      'Null' => 'YES',
		      'Key' => '',
		      'Default' => NULL,
		    ),
		    'Fav' => 
		    array (
		      'Type' => 'int(255)',
		      'Null' => 'YES',
		      'Key' => '',
		      'Default' => NULL,
		    ),
		    'Message' => 
		    array (
		      'Type' => 'int(255)',
		      'Null' => 'YES',
		      'Key' => '',
		      'Default' => NULL,
		    ),
		    'LauchDate' => 
		    array (
		      'Type' => 'date',
		      'Null' => 'NO',
		      'Key' => 'PRI',
		      'Default' => NULL,
		    ),
		    'StatisticDate' => 
		    array (
		      'Type' => 'date',
		      'Null' => 'YES',
		      'Key' => '',
		      'Default' => NULL,
		    ),
		  ),
		  'Primary' => 
		  array (
		    0 => 'LauchDate',
		  ),
		  'auto' => '',
		  'charset' => 'utf8',
		);
		
		$this->tableFieldHash = array();
		$this->primaryKey = $tFields['Primary'];
		$this->autoKey = $tFields['auto'];
		$this->charset = $tFields['charset'];
		$columns = new ArrayObject(array_keys($tFields['Fields']));
		foreach ($columns as $k=>$v){
			$key = $this->genKey($v);
			$this->{$key} = &$this->hashMap[$v];
			$field = $this->criteria->isIgnoreCase() ? strtolower($v) : $v;
			$this->tableFieldHash[$field] = array('Column'=>$v)+$tFields['Fields'][$v];
		}
		$this->mSQLCommand->setCharset($this->charset);
	}
}
?>