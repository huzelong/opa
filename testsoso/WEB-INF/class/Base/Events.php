<?php
/**
 * @author moonzhang (moonzhang@sogou-inc.com)
 * @version v1.0 2017-04-19 19:01:12 PM
 * Base table class : Events - (utf8)
 */

class Base_Events extends SOSO_ORM_Table/*Object*/ {
	/**
	 * 
	 * Class Member Mapping to Table "Events" Field "EventId"
	 * Refer to $this->mMapHash['EventId'];
	 * @access public
	 * @var int(11)
	*/
	public $mEventId;

	/**
	 * 
	 * Class Member Mapping to Table "Events" Field "EventName"
	 * Refer to $this->mMapHash['EventName'];
	 * @access public
	 * @var varchar(255)
	*/
	public $mEventName;

	/**
	 * 
	 * Class Member Mapping to Table "Events" Field "EventPersonId"
	 * Refer to $this->mMapHash['EventPersonId'];
	 * @access public
	 * @var int(11)
	*/
	public $mEventPersonId;

	/**
	 * 
	 * Class Member Mapping to Table "Events" Field "EventCategoryId"
	 * Refer to $this->mMapHash['EventCategoryId'];
	 * @access public
	 * @var int(11)
	*/
	public $mEventCategoryId;

	/**
	 * 
	 * Class Member Mapping to Table "Events" Field "EventTime"
	 * Refer to $this->mMapHash['EventTime'];
	 * @access public
	 * @var date
	*/
	public $mEventTime;

	/**
	 * 
	 * Class Member Mapping to Table "Events" Field "EventDiscription"
	 * Refer to $this->mMapHash['EventDiscription'];
	 * @access public
	 * @var varchar(255)
	*/
	public $mEventDiscription;

	/**
	 * 
	 * Class Member Mapping to Table "Events" Field "Location"
	 * Refer to $this->mMapHash['Location'];
	 * @access public
	 * @var varchar(255)
	*/
	public $mLocation;

	
	public function __construct($pTable="Events",$pIndex="0") {
		parent::__construct($pTable,$pIndex);
	}
	
	public function prepareHashMap(){
		$tFields = array (
		  'Fields' => 
		  array (
		    'EventId' => 
		    array (
		      'Type' => 'int(11)',
		      'Null' => 'NO',
		      'Key' => 'PRI',
		      'Default' => NULL,
		    ),
		    'EventName' => 
		    array (
		      'Type' => 'varchar(255)',
		      'Null' => 'YES',
		      'Key' => '',
		      'Default' => NULL,
		    ),
		    'EventPersonId' => 
		    array (
		      'Type' => 'int(11)',
		      'Null' => 'NO',
		      'Key' => 'MUL',
		      'Default' => NULL,
		    ),
		    'EventCategoryId' => 
		    array (
		      'Type' => 'int(11)',
		      'Null' => 'YES',
		      'Key' => 'MUL',
		      'Default' => NULL,
		    ),
		    'EventTime' => 
		    array (
		      'Type' => 'date',
		      'Null' => 'YES',
		      'Key' => '',
		      'Default' => NULL,
		    ),
		    'EventDiscription' => 
		    array (
		      'Type' => 'varchar(255)',
		      'Null' => 'YES',
		      'Key' => '',
		      'Default' => NULL,
		    ),
		    'Location' => 
		    array (
		      'Type' => 'varchar(255)',
		      'Null' => 'YES',
		      'Key' => '',
		      'Default' => NULL,
		    ),
		  ),
		  'Primary' => 
		  array (
		    0 => 'EventId',
		  ),
		  'auto' => 'EventId',
		  'charset' => 'utf8',
		);
		
		$this->tableFieldHash = array();
		$this->primaryKey = $tFields['Primary'];
		$this->autoKey = $tFields['auto'];
		$this->charset = $tFields['charset'];
		$columns = new ArrayObject(array_keys($tFields['Fields']));
		foreach ($columns as $k=>$v){
			$key = $this->genKey($v);
			$this->{$key} = &$this->hashMap[$v];
			$field = $this->criteria->isIgnoreCase() ? strtolower($v) : $v;
			$this->tableFieldHash[$field] = array('Column'=>$v)+$tFields['Fields'][$v];
		}
		$this->mSQLCommand->setCharset($this->charset);
	}
}
?>