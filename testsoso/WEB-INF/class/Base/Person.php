<?php
/**
 * @author moonzhang (moonzhang@sogou-inc.com)
 * @version v1.0 2017-04-19 19:01:12 PM
 * Base table class : Person - (utf8)
 */

class Base_Person extends SOSO_ORM_Table/*Object*/ {
	/**
	 * 
	 * Class Member Mapping to Table "Person" Field "PersonId"
	 * Refer to $this->mMapHash['PersonId'];
	 * @access public
	 * @var int(11)
	*/
	public $mPersonId;

	/**
	 * 
	 * Class Member Mapping to Table "Person" Field "PersonName"
	 * Refer to $this->mMapHash['PersonName'];
	 * @access public
	 * @var varchar(255)
	*/
	public $mPersonName;

	/**
	 * 
	 * Class Member Mapping to Table "Person" Field "TeamId"
	 * Refer to $this->mMapHash['TeamId'];
	 * @access public
	 * @var int(11)
	*/
	public $mTeamId;

	
	public function __construct($pTable="Person",$pIndex="0") {
		parent::__construct($pTable,$pIndex);
	}
	
	public function prepareHashMap(){
		$tFields = array (
		  'Fields' => 
		  array (
		    'PersonId' => 
		    array (
		      'Type' => 'int(11)',
		      'Null' => 'NO',
		      'Key' => 'PRI',
		      'Default' => NULL,
		    ),
		    'PersonName' => 
		    array (
		      'Type' => 'varchar(255)',
		      'Null' => 'NO',
		      'Key' => '',
		      'Default' => NULL,
		    ),
		    'TeamId' => 
		    array (
		      'Type' => 'int(11)',
		      'Null' => 'NO',
		      'Key' => 'MUL',
		      'Default' => NULL,
		    ),
		  ),
		  'Primary' => 
		  array (
		    0 => 'PersonId',
		  ),
		  'auto' => 'PersonId',
		  'charset' => 'utf8',
		);
		
		$this->tableFieldHash = array();
		$this->primaryKey = $tFields['Primary'];
		$this->autoKey = $tFields['auto'];
		$this->charset = $tFields['charset'];
		$columns = new ArrayObject(array_keys($tFields['Fields']));
		foreach ($columns as $k=>$v){
			$key = $this->genKey($v);
			$this->{$key} = &$this->hashMap[$v];
			$field = $this->criteria->isIgnoreCase() ? strtolower($v) : $v;
			$this->tableFieldHash[$field] = array('Column'=>$v)+$tFields['Fields'][$v];
		}
		$this->mSQLCommand->setCharset($this->charset);
	}
}
?>