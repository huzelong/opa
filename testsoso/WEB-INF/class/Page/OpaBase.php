<?php
class Page_OpaBase extends SOSO_Page {
	const EVENT_TYPE_ARTICLE = 1;
    const EVENT_TYPE_QA_INNER = 2;
	const EVENT_TYPE_QA_OUTER = 3;
	const EVENT_TYPE_SHARE_INNER = 5;
	const EVENT_TYPE_SHARE_OUTER = 4;
	
    public function __construct() {
	parent::__construct();
        $this->mRequest = array_merge($this->mGET, $this->mPOST);
    }
//trans要转换的玩数组
//reference有对应关系的数组
//要替换的字段名字
//被替换的字段Id
//被替换的字段名字
    public function convertflection($trans,$reference,$origial,$replaceId,$repalceWord){
                for($j=0;$j<count($trans);$j++){
                         $event = $trans[$j];
                         $eid = $event["$origial"];
                        for ($i=0;$i<count($reference);$i++){
                         $temp=$reference[$i];
                         if($temp[$replaceId]==$eid){
                                $trans[$j][$origial]=$temp[$repalceWord];
                                }
                        }
                }
       return $trans;
        }
	public function FillArticleName($events){
		$SqlCommand = SOSO_DB_SQLCommand::getInstance(0);
		$SqlCommand->setCharset("utf8");
		$dataNum = $SqlCommand->ExecuteArrayQuery("SELECT COUNT(*) FROM `Events`")[0]['COUNT(*)'];
		
		$config=array("filename"=>"temp/log/testsoso.log","level"=>SOSO_Log::DEBUG);
		$log=SOSO_Log::getLogging()->basicConfig($config);
		
		for($j=0;$j<count($events);$j++){
			$event = $events[$j];
			if($event["EventName"])
				continue;
			$etime = $event["EventTime"];
			$artcleTitle = $SqlCommand->ExecuteArrayQuery("SELECT ArticleTitle from Article where LauchDate='".$etime."'");
			$log->debug("替换文章标题",$artcleTitle[0]);
			$title = $artcleTitle[0]["ArticleTitle"];
			if(!$title)
				$title="该日期无对应文章";
			$events[$j]["EventName"] = $title;
			$events[$j]["Location"] = "SOGOUQA";
			$events[$j]["EventDiscription"] ="发表公众号";
			}
		return $events;
		}
	public function datedeal($starttime,$stoptime){
			$datestart = new DateTime($starttime);
			$datestart = new DateTime($starttime);
			$datestop = new DateTime($stoptime);
			$interval = $datestart->diff($datestop);//相差天数
			$dateearly = $datestart->sub($interval);
			$earlytime = $dateearly->format('Y-m-d') . "\n";
			$datearr = array('starttime'=>$starttime,'stoptime'=>$stoptime,'earlytime'=>$earlytime,'interval'=>$interval->days);
			return $datearr;

		}
	public function getteamname($teamid){
			$oTeam= new Team();
			$oTeam->add('TeamId',$teamid);
			$data = $oTeam->find();
		return $data;
	}

	public function getteamarr(){

			$oTeam = new Team();
			$data = $oTeam->find();
		return $data;
	}
	
	
	
	public function getarticles($starttime,$stoptime){
			$oArticle = new Article();
			$oArticle->add(SOSO_ORM_Restrictions::between('LauchDate',$starttime,$stoptime));
			$data = $oArticle->find();
			return $data;
	}
	public function getteamnum($teamid){
			$oPerson=new Person();
			$oPerson->add('TeamId',$teamid);
			$data = $oPerson->find();
			return $data;
	}
	public function getallpeople(){
			$oPerson=new Person();
			$data = $oPerson->find();
			return $data;
	}
	
	
	public function getevents($starttime,$stoptime){
			$oEvents = new Events();
			$oEvents->add(SOSO_ORM_Restrictions::between('EventTime',$starttime,$stoptime));
			$data = $oEvents->find();
			return $data;
	}
	
	
	
	
	
	public function getintersection($events,$people,$articles,$cateid){
		if($cateid==SELF::EVENT_TYPE_ARTICLE ){
			$personids = array();
			if($this->is_assoc($people)){
				array_push($personids,$people["PersonId"]);
			}else{
				foreach($people as $personone){
				array_push($personids,$personone["PersonId"]);
				}
			}
			$arr=array();
				for($j=0;$j<count($events);$j++){
					$event = $events[$j];
					$cate=$event["EventCategoryId"];
					$id = $event["EventPersonId"];
					if($cate==SELF::EVENT_TYPE_ARTICLE && in_array($id,$personids)){
				
						$etime = $event["EventTime"];
						for($i=0;$i<count($articles);$i++){
									$article=$articles[$i];
									if($article["LauchDate"]==$etime){
										array_push($arr,$article);
									}
								}

							 }

				}
		   return $arr;
		}
		
		if($cateid==SELF::EVENT_TYPE_QA_INNER){
			$personids = array();
			if($this->is_assoc($people)){
				array_push($personids,$people["PersonId"]);
			
			}else{
				foreach($people as $personone){
				array_push($personids,$personone["PersonId"]);
				}
			}
			$arr=array();
				for($j=0;$j<count($events);$j++){
					$event = $events[$j];
					$cate=$event["EventCategoryId"];
					$id = $event["EventPersonId"];
					if($cate==SELF::EVENT_TYPE_QA_INNER && in_array($id,$personids)){
						array_push($arr,$event);

							 }

				}
		   return $arr;
		}	
		
		if($cateid==SELF::EVENT_TYPE_QA_OUTER){
			$personids = array();
			if($this->is_assoc($people)){
				array_push($personids,$people["PersonId"]);
			
			}else{
				foreach($people as $personone){
				array_push($personids,$personone["PersonId"]);
				}
			}
			$arr=array();
				for($j=0;$j<count($events);$j++){
					$event = $events[$j];
					$cate=$event["EventCategoryId"];
					$id = $event["EventPersonId"];
					if($cate==SELF::EVENT_TYPE_QA_OUTER && in_array($id,$personids)){
						array_push($arr,$event);

							 }

				}
		   return $arr;
			
		}
		
		if($cateid==SELF::EVENT_TYPE_SHARE_INNER){
			$personids = array();
			if($this->is_assoc($people)){
				array_push($personids,$people["PersonId"]);
			
			}else{
				foreach($people as $personone){
				array_push($personids,$personone["PersonId"]);
				}
			}
			$arr=array();
				for($j=0;$j<count($events);$j++){
					$event = $events[$j];
					$cate=$event["EventCategoryId"];
					$id = $event["EventPersonId"];
					if($cate==SELF::EVENT_TYPE_SHARE_INNER && in_array($id,$personids)){
						array_push($arr,$event);
						}

				}
		   return $arr;
		}
		if($cateid==SELF::EVENT_TYPE_SHARE_OUTER){
				$personids = array();
			if($this->is_assoc($people)){
				array_push($personids,$people["PersonId"]);
			
			}else{
				foreach($people as $personone){
				array_push($personids,$personone["PersonId"]);
				}
			}
			$arr=array();
				for($j=0;$j<count($events);$j++){
					$event = $events[$j];
					$cate=$event["EventCategoryId"];
					$id = $event["EventPersonId"];
					if($cate==SELF::EVENT_TYPE_SHARE_OUTER && in_array($id,$personids)){
						array_push($arr,$event);

							 }

				}
		   return $arr;
		}
		
		return false;
		
		
	}
	
	public function is_assoc($arr) {  
		return array_keys($arr) !== range(0, count($arr) - 1);  
	}  
	
	
	public function caculate($predata){
			$num = count($predata);
			$fav=0;
			$share=0;
			$reprint=0;
			$message=0;
			$pageviews=0;
			
			foreach ($predata as $one){
				foreach($one as $key =>$value){
		
					if($key=="PageViews"){
						$pageviews=$pageviews+(int)$value;
						}
					if($key=="Reprint"){
						$reprint=$reprint+(int)$value;
					}
					if($key=="Share"){
						$share=$share+(int)$value;
					}
					if($key=="Message"){
						$message=$message+(int)$value;
					}
					if($key=="Fav"){
						$fav=$fav+(int)$value;
					}
				}
			}
			$afterdata=array("num"=>$num,"fav"=>$fav,"share"=>$share,"reprint"=>$reprint,"message"=>$message,"pageviews"=>$pageviews);
		return $afterdata;
	}
	
	
	
	public function getrate($after,$early){

		$rate=array();
		foreach($after as $item =>$value){
			if($early[$item] == 0 || $after[$item] == 0){//分母为零
				$temp = $after[$item];
				$tempstr = "某阶段没有数据";
			}else{
			$temp = ($after[$item]- $early[$item])/$early[$item];
			$temp = round($temp, 3)*100;
			$tempstr = $temp."%";
			}
			$rate[$item]=$tempstr;
		}
		if($early["num"]===0){
		$tempratestr="上次没有数据";
		}elseif($after["num"]===0){
			$tempratestr="这次没有数据";
		}else{
		$early_article_avg_pv=$early["pageviews"]/$early["num"];
		$last_article_avg_pv=$after["pageviews"]/$after["num"];
		$temprate =  round(($last_article_avg_pv-$early_article_avg_pv)/$early_article_avg_pv,3)*100;
		$tempratestr = $temprate."%";
		}
		//加入上次发文的数据
		$rate["singleArticleGrate"]=$tempratestr;
		$rate["lastArticleNum"] = $early["num"];
		$rate["lastArticlePageview"]=$early["pageviews"];
		return $rate;	
	}
	
	//jisuande
	//jisuande
	//jisuande
	public function adjustformat($teamarr){
		$data = array();
		foreach($teamarr as $one){
			$temp = array();
			$title = $one["TeamName"];
			$wenzhang = $one[0]["wenzhang"];
			
			$pageviews = $wenzhang["pageviews"];
			$reprint = $wenzhang["reprint"];
			$fav = $wenzhang["fav"];
			$message = $wenzhang["message"];
			$share = $wenzhang["share"];
			$neibudayi= $one[1]["neibudayi"]["num"];
			$waibudayi= $one[2]["waibudayi"]["num"];
			$neibufenxiang= $one[3]["neibufenxiang"]["num"];
			$waibufenxiang= $one[4]["waibufenxiang"]["num"];
			$temp["teamname"] = $title;
			$temp["pageviews"]=$pageviews;
			$temp["reprint"]=$reprint;
			$temp["fav"]=$fav;
			$temp["message"]=$message;
			$temp["share"]=$share;
			$temp["neibudayi"]=$neibudayi;
			$temp["waibudayi"]=$waibudayi;
			$temp["neibufenxiang"]=$neibufenxiang;
			$temp["waibufenxiang"]=$waibufenxiang;
			array_push($data,$temp);
			
		}
			return $data;
	}
	public function adjustformat_person($people){
		//var_dump($people);
		   $data = array();
		foreach($people as $one){
			$temp = array();
			$personename = $one["PersonName"];
			$wenzhang = $one[0];
			$pagenum = $wenzhang["num"];
			$pageviews = $wenzhang["pageviews"];
			$reprint = $wenzhang["reprint"];
			$fav = $wenzhang["fav"];
			$message = $wenzhang["message"];
			$share = $wenzhang["share"];
			$neibudayi= $one[1]["num"];
			$waibudayi= $one[2]["num"];
			$neibufenxiang= $one[3]["num"];
			$waibufenxiang= $one[4]["num"];
			$wenzhangzengliang =$one[5]["wenzhang"]["pageviews"];
			$singleArticleGrate =$one[5]["wenzhang"]["singleArticleGrate"];
			$lastArticleNum =$one[5]["wenzhang"]["lastArticleNum"];
			$lastArticlePageview =$one[5]["wenzhang"]["lastArticlePageview"];
			$temp["pagenum"] = $pagenum;
			$temp["personename"] = $personename;
			$temp["pageviews"]=$pageviews;
			$temp["reprint"]=$reprint;
			$temp["fav"]=$fav;
			$temp["message"]=$message;
			$temp["share"]=$share;
			$temp["neibudayi"]=$neibudayi;
			$temp["waibudayi"]=$waibudayi;
			$temp["neibufenxiang"]=$neibufenxiang;
			$temp["waibufenxiang"]=$waibufenxiang;
			$temp["wenzhangzengliang"]=$wenzhangzengliang;
			$temp["singleArticleGrate"]=$singleArticleGrate;
			$temp["lastArticleNum"]=$lastArticleNum;
			$temp["lastArticlePageview"]=$lastArticlePageview;

			array_push($data,$temp);
			
		}
			return $data;
	}
	
	public function adjustformat_person_top($people){		
			$data = array();
			for($i =0;$i<count($people);$i++){
			$personename = $people[$i]["PersonName"];
			for($x=0;$x<count($people[$i][0]);$x++) {
				$people[$i][0][$x]["personname"]=$personename;
				}	
			}
			foreach($people as $person){
				for($h=0;$h<count($person[0]);$h++){
					array_push($data,$person[0][$h]);
				}
			}
			return $data;
		}
		
	
	
	public function score($teamdata){
		$score = array();
		for($i = 0;$i<count($teamdata);$i++ ){
			
			$one=$teamdata[$i];
			
			$temp=array();
			$teamname = $one["teamname"];
			
			$pageviews= (float)$one["pageviews"]/100;
			$reprint= (float)$one["reprint"]/100;
			$fav= (float)$one["fav"]/100;
			$message= (float)$one["message"]/100;
			$share= (float)$one["share"]/100;
			$neibudayi= $one["neibudayi"];
			$waibudayi= $one["waibudayi"];
			$neibufenxiang= $one["neibufenxiang"];
			$waibufenxiang= $one["waibufenxiang"];
			
			$pageviewsscore = $this->getscore($pageviews);

			$reprintscore = $this->getscore($reprint);
			$favscore = $this->getscore($fav);
			$messagescore = $this->getscore($message);
			$sharescore = $this->getscore($share);
//初始十分,和每一除了它之外的数比较，有一个大于减2分。			
			$neibudayiscore = 10;
			for($j = 0;$j<count($teamdata);$j++ ){
				$x = $teamdata[$j]["neibudayi"];
				if($i!==$j){
					if($neibudayi<$x){
						$neibudayiscore=$neibudayiscore-2;
					}
				}
			}
			
			$waibudayiscore = 5;
			for($j = 0;$j<count($teamdata);$j++ ){
				$x = $teamdata[$j]["waibudayi"];
				if($i!==$j){
					if($waibudayi<$x){
						$waibudayiscore=$waibudayiscore-1;
					}
				}
			}
			$neibufenxiangscore = 15;
			for($j = 0;$j<count($teamdata);$j++ ){
				$x = $teamdata[$j]["neibufenxiang"];
				if($i!==$j){
					if($neibufenxiang<$x){
						$neibufenxiangscore=$neibufenxiangscore-3;
					}
				}
			}
			$waibufenxiangscore = 15;
			for($j = 0;$j<count($teamdata);$j++ ){
				$x = $teamdata[$j]["waibufenxiang"];
				if($i!==$j){
					if($waibufenxiang<$x){
						$waibufenxiangscore=$waibufenxiangscore-3;
					}
				}
			}

			if($neibudayi==0){$neibudayiscore=0;}
			if($waibudayi==0){$waibudayiscore=0;}
			if($neibufenxiang==0){$neibufenxiangscore=0;}
			if($waibufenxiang==0){$waibufenxiangscore=0;}
			
			$total = $pageviewsscore+$reprintscore+$favscore+$messagescore+$sharescore +$neibudayiscore +$waibudayiscore+$neibufenxiangscore+$waibufenxiangscore;
			
			$temp["teamname"] = $teamname;
			$temp["pageviews"]=$pageviewsscore;
			$temp["reprint"]=$reprintscore;
			$temp["fav"]=$favscore;
			$temp["message"]=$messagescore;
			$temp["share"]=$sharescore;
			$temp["neibudayi"]=$neibudayiscore;
			$temp["waibudayi"]=$waibudayiscore;
			$temp["neibufenxiang"]=$neibufenxiangscore;
			$temp["waibufenxiang"]=$waibufenxiangscore;
			$temp["total"]=$total;
			array_push($score,$temp);
		}
		
		return $score;
	}
	public function getscore($origial){
		if($origial<=0.05){
			return 1;
		}
		if($origial<=0.2){
			return 2;
		}
		if($origial<=0.4){
			return 3;
		}
		if($origial<=0.65){
			return 5;
		}
		if($origial>0.65){
			return 7;
		}
	}

	function user_realip() {
        if (getenv('HTTP_CLIENT_IP'))
        {
            $ip = getenv('HTTP_CLIENT_IP');
        } elseif (getenv('HTTP_X_FORWARDED_FOR'))
        {
            $ip = getenv('HTTP_X_FORWARDED_FOR');
        } elseif (getenv('REMOTE_ADDR'))
        {
            $ip = getenv('REMOTE_ADDR');
        } else
        {
            $ip = $HTTP_SERVER_VARS['REMOTE_ADDR'];
        }
        return $ip;
    }
	
	
	
	
}
