<?php
class Page_User_OpaArticles extends SOSO_Page{
    public function run(){
	parent::__construct();
	$numPerPage = 60;	
	$page  = isset($this->mGET['page'])? $this->mGET['page'] : '1'; 
	//echo $numPerPage;
	$SqlCommand = SOSO_DB_SQLCommand::getInstance(0);
	$SqlCommand->setCharset("utf8");
	$dataNum = $SqlCommand->ExecuteArrayQuery("SELECT COUNT(*) FROM `Article`")[0]['COUNT(*)'];//计算总量
	$pageNum = ceil($dataNum/$numPerPage);//判断有多少页
	if($page>$pageNum){
		return false;
	}
	$this->assign("pageNum",$pageNum);//前端循环分页
	
	$arr=$SqlCommand->ExecuteArrayQuery("select * from Article order by LauchDate desc",$page,$numPerPage,'assoc');
	$this->assign("articles", $arr);
        $res = $this->fetch('tpl.articles.htm');
	echo $res;


    }
}

