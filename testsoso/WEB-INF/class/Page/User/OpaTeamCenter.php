<?php
class Page_User_OpaTeamCenter extends Page_OpaBase{
    protected $roomPerPage = 50;

    public function __construct(){
        parent::__construct();
    }
    public function run(){
	$teamid  = isset($this->mPOST['teamid'])? $this->mPOST['teamid'] : '1'; 
	$starttime = isset($this->mPOST['starttime'])? $this->mPOST['starttime'] : '2017-02-01'; 
	$stoptime = isset($this->mPOST['stoptime'])? $this->mPOST['stoptime'] : '2017-04-21'; 	

	
	
	$datedeal = $this->datedeal($starttime,$stoptime);
	$people = $this->getteamnum($teamid);
	$teamname = $this ->getteamname($teamid);
	$events = $this->getevents($starttime,$stoptime);
	$eventsearly = $this->getevents($datedeal["earlytime"],$datedeal["starttime"]);
//文章质量
//最近的时间
	$articles = $this->getarticles($starttime,$stoptime);
	$predata = $this->getintersection($events,$people,$articles,SELF::EVENT_TYPE_ARTICLE);
	$afterdata = $this->caculate($predata);
//更早的时间
	$articlesearly = $this->getarticles($datedeal["earlytime"],$datedeal["starttime"]);
	$predataearly = $this->getintersection($eventsearly,$people,$articlesearly,SELF::EVENT_TYPE_ARTICLE);
	$afterdataearly = $this->caculate($predataearly);
	$articlesgrate=$this->getrate($afterdata,$afterdataearly);
	
	
//答疑_粉丝
	//最近的时间
	$QAINNER_predata = $this->getintersection($events,$people,NULL,SELF::EVENT_TYPE_QA_INNER);
	$QAINNER_afterdata = $this->caculate($QAINNER_predata);

	//更早的时间
	$QAINNER_predata_early = $this->getintersection($eventsearly,$people,NULL,SELF::EVENT_TYPE_QA_INNER);
	$QAINNER_afterdata_early = $this->caculate($QAINNER_predata_early);

	$QAgrate_inner=$this->getrate($QAINNER_afterdata,$QAINNER_afterdata_early);
//答疑_外部
	//最近的时间
	$QAOUTER_predata = $this->getintersection($events,$people,NULL,SELF::EVENT_TYPE_QA_OUTER);
	$QAOUTER_afterdata = $this->caculate($QAOUTER_predata);

	//更早的时间
	$QAOUTER_predata_early = $this->getintersection($eventsearly,$people,NULL,SELF::EVENT_TYPE_QA_OUTER);
	$QAOUTER_afterdata_early = $this->caculate($QAOUTER_predata_early);

	$QAgrate_outer=$this->getrate($QAOUTER_afterdata,$QAOUTER_afterdata_early);


//分享_公司
	//最近的时间
	$SHAREINNER_predata = $this->getintersection($events,$people,NULL,SELF::EVENT_TYPE_SHARE_INNER);
	$SHAREINNER_afterdata = $this->caculate($SHAREINNER_predata);

	//更早的时间
	$SHAREINNER_predata_early = $this->getintersection($eventsearly,$people,NULL,SELF::EVENT_TYPE_SHARE_INNER);
	$SHAREINNER_afterdata_early = $this->caculate($SHAREINNER_predata_early);

	$SHAREgrate_inner=$this->getrate($SHAREINNER_afterdata,$SHAREINNER_afterdata_early);
//分享_外部
	//最近的时间
	$SHAREOUTER_predata = $this->getintersection($events,$people,NULL,SELF::EVENT_TYPE_SHARE_OUTER);
	$SHAREOUTER_afterdata = $this->caculate($SHAREOUTER_predata);

	//更早的时间
	$SHAREOUTER_predata_early = $this->getintersection($eventsearly,$people,NULL,SELF::EVENT_TYPE_SHARE_OUTER);
	$SHAREOUTER_afterdata_early = $this->caculate($SHAREOUTER_predata_early);

	$SHAREgrate_outer=$this->getrate($SHAREOUTER_afterdata,$SHAREOUTER_afterdata_early);

	

//文章
	$this->assign("datedeal", $datedeal);
	$this->assign("statistic", $afterdata);
	$this->assign("teamname", $teamname[0]);
	$this->assign("statistic2", $afterdataearly);
	$this->assign("articlesgrate", $articlesgrate);
//答疑_粉丝
	$this->assign("QAINNER_afterdata_early", $QAINNER_afterdata_early);
	$this->assign("QAINNER_afterdata", $QAINNER_afterdata);
	$this->assign("QAgrate_inner", $QAgrate_inner);
//答疑_外部
	$this->assign("QAOUTER_afterdata_early", $QAOUTER_afterdata_early);
	$this->assign("QAOUTER_afterdata", $QAOUTER_afterdata);
	$this->assign("QAgrate_outer", $QAgrate_outer);
//分享_公司
	$this->assign("SHAREINNER_afterdata_early", $SHAREINNER_afterdata_early);
	$this->assign("SHAREINNER_afterdata", $SHAREINNER_afterdata);
	$this->assign("SHAREgrate_inner", $SHAREgrate_inner);
//分享_外部
	$this->assign("SHAREOUTER_afterdata_early", $SHAREOUTER_afterdata_early);
	$this->assign("SHAREOUTER_afterdata", $SHAREOUTER_afterdata);
	$this->assign("SHAREgrate_outer", $SHAREgrate_outer);
	

	
	
	
    $res = $this->fetch('tpl.teamcenter.htm');
	echo $res;
    }
 
}
