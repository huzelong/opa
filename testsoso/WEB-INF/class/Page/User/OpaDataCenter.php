<?php
class Page_User_OpaDataCenter extends Page_OpaBase{
    protected $roomPerPage = 50;

    public function __construct(){
        parent::__construct();
    }
    public function run(){
	$starttime = isset($this->mPOST['starttime'])? $this->mPOST['starttime'] : '2017-05-01'; 
	$stoptime = isset($this->mPOST['stoptime'])? $this->mPOST['stoptime'] : '2017-07-20'; 	


	
	
	$datedeal = $this->datedeal($starttime,$stoptime);
	$events = $this->getevents($starttime,$stoptime);

	$eventsearly = $this->getevents($datedeal["earlytime"],$datedeal["starttime"]);
	
	$teamarr = $this->getteamarr();
//团队数据
	for ($i = 0;$i < count($teamarr);$i++){
		$teamid =  $teamarr[$i]["TeamId"];
		$teamname = $this ->getteamname($teamid);
		$people = $this->getteamnum($teamid);

		//文章质量
		//最近的时间
			$articles = $this->getarticles($starttime,$stoptime);
			$predata = $this->getintersection($events,$people,$articles,SELF::EVENT_TYPE_ARTICLE);
			$afterdata = $this->caculate($predata);
	
		//更早的时间
			$articlesearly = $this->getarticles($datedeal["earlytime"],$datedeal["starttime"]);
			$predataearly = $this->getintersection($eventsearly,$people,$articlesearly,SELF::EVENT_TYPE_ARTICLE);
			$afterdataearly = $this->caculate($predataearly);
			
			$articlesgrate=$this->getrate($afterdata,$afterdataearly);
			$wenzhang = array("wenzhang"=>$articlesgrate);

			
			
		//答疑_粉丝
			//最近的时间
			
			$QAINNER_predata = $this->getintersection($events,$people,NULL,SELF::EVENT_TYPE_QA_INNER);
			
			$QAINNER_afterdata = $this->caculate($QAINNER_predata);

			//更早的时间
			$QAINNER_predata_early = $this->getintersection($eventsearly,$people,NULL,SELF::EVENT_TYPE_QA_INNER);
			$QAINNER_afterdata_early = $this->caculate($QAINNER_predata_early);

			$QAgrate_inner=$this->getrate($QAINNER_afterdata,$QAINNER_afterdata_early);

			$neibudayi = array("neibudayi"=>$QAINNER_afterdata);
		//答疑_外部
			//最近的时间
			$QAOUTER_predata = $this->getintersection($events,$people,NULL,SELF::EVENT_TYPE_QA_OUTER);
			$QAOUTER_afterdata = $this->caculate($QAOUTER_predata);

			
			//更早的时间
			$QAOUTER_predata_early = $this->getintersection($eventsearly,$people,NULL,SELF::EVENT_TYPE_QA_OUTER);
			$QAOUTER_afterdata_early = $this->caculate($QAOUTER_predata_early);

			$QAgrate_outer=$this->getrate($QAOUTER_afterdata,$QAOUTER_afterdata_early);
			$waibudayi = array("waibudayi"=>$QAOUTER_afterdata);
			
		//分享_公司
			//最近的时间
			$SHAREINNER_predata = $this->getintersection($events,$people,NULL,SELF::EVENT_TYPE_SHARE_INNER);
			$SHAREINNER_afterdata = $this->caculate($SHAREINNER_predata);

			//更早的时间
			$SHAREINNER_predata_early = $this->getintersection($eventsearly,$people,NULL,SELF::EVENT_TYPE_SHARE_INNER);
			$SHAREINNER_afterdata_early = $this->caculate($SHAREINNER_predata_early);

			$SHAREgrate_inner=$this->getrate($SHAREINNER_afterdata,$SHAREINNER_afterdata_early);
			$neibufenxiang = array("neibufenxiang"=>$SHAREINNER_afterdata);
		//分享_外部
			//最近的时间
			$SHAREOUTER_predata = $this->getintersection($events,$people,NULL,SELF::EVENT_TYPE_SHARE_OUTER);
			$SHAREOUTER_afterdata = $this->caculate($SHAREOUTER_predata);

			//更早的时间
			$SHAREOUTER_predata_early = $this->getintersection($eventsearly,$people,NULL,SELF::EVENT_TYPE_SHARE_OUTER);
			$SHAREOUTER_afterdata_early = $this->caculate($SHAREOUTER_predata_early);

			$SHAREgrate_outer=$this->getrate($SHAREOUTER_afterdata,$SHAREOUTER_afterdata_early);
			$waibufenxiang = array("waibufenxiang"=>$SHAREOUTER_afterdata);

			array_push($teamarr[$i],$wenzhang,$neibudayi,$waibudayi,$neibufenxiang,$waibufenxiang);
			
			
			}
		
			
//个人数据_总量
		$all = $this->getallpeople();
	for ($i = 0;$i < count($all);$i++){
		
		$afterdata;
		$QAINNER_afterdata;
		$QAOUTER_afterdata;
		$SHAREINNER_afterdata;
		$SHAREOUTER_afterdata;
		
		
		
		$people = $all[$i];

		//文章质量
		//最近的时间
			$articles = $this->getarticles($starttime,$stoptime);
			
		
			
			$predata = $this->getintersection($events,$people,$articles,SELF::EVENT_TYPE_ARTICLE);
			$afterdata = $this->caculate($predata);

			
		//更早的时间
			$articlesearly = $this->getarticles($datedeal["earlytime"],$datedeal["starttime"]);
			$predataearly = $this->getintersection($eventsearly,$people,$articlesearly,SELF::EVENT_TYPE_ARTICLE);
		    $afterdataearly = $this->caculate($predataearly);
	
			$articlesgrate=$this->getrate($afterdata,$afterdataearly);
			//["num"]=> int(0) ["fav"]=> int(0) ["share"]=> int(0) ["reprint"]=> int(0) ["message"]=> int(0) ["pageviews"]=> int(0)
	
			$wenzhang = array("wenzhang"=>$articlesgrate);
			
			//

			
			
		//答疑_粉丝
			//最近的时间
			
			$QAINNER_predata = $this->getintersection($events,$people,NULL,SELF::EVENT_TYPE_QA_INNER);
			
			$QAINNER_afterdata = $this->caculate($QAINNER_predata);

			
		
			//更早的时间
		//	$QAINNER_predata_early = $this->getintersection($eventsearly,$people,NULL,SELF::EVENT_TYPE_QA_INNER);
		//	$QAINNER_afterdata_early = $this->caculate($QAINNER_predata_early);

			//$QAgrate_inner=$this->getrate($QAINNER_afterdata,$QAINNER_afterdata_early);

			//$neibudayi = array("neibudayi"=>$QAgrate_inner);
		//答疑_外部
			//最近的时间
			$QAOUTER_predata = $this->getintersection($events,$people,NULL,SELF::EVENT_TYPE_QA_OUTER);
			$QAOUTER_afterdata = $this->caculate($QAOUTER_predata);

			//更早的时间
		//	$QAOUTER_predata_early = $this->getintersection($eventsearly,$people,NULL,SELF::EVENT_TYPE_QA_OUTER);
		//	$QAOUTER_afterdata_early = $this->caculate($QAOUTER_predata_early);

		//	$QAgrate_outer=$this->getrate($QAOUTER_afterdata,$QAOUTER_afterdata_early);
		//	$waibudayi = array("waibudayi"=>$QAgrate_outer);

		//分享_公司
			//最近的时间
			$SHAREINNER_predata = $this->getintersection($events,$people,NULL,SELF::EVENT_TYPE_SHARE_INNER);
			$SHAREINNER_afterdata = $this->caculate($SHAREINNER_predata);

			//更早的时间
		//	$SHAREINNER_predata_early = $this->getintersection($eventsearly,$people,NULL,SELF::EVENT_TYPE_SHARE_INNER);
		//	$SHAREINNER_afterdata_early = $this->caculate($SHAREINNER_predata_early);

		//	$SHAREgrate_inner=$this->getrate($SHAREINNER_afterdata,$SHAREINNER_afterdata_early);
		//	$neibufenxiang = array("neibufenxiang"=>$SHAREgrate_inner);
		//分享_外部
			//最近的时间
			$SHAREOUTER_predata = $this->getintersection($events,$people,NULL,SELF::EVENT_TYPE_SHARE_OUTER);
			$SHAREOUTER_afterdata = $this->caculate($SHAREOUTER_predata);

			//更早的时间
		//	$SHAREOUTER_predata_early = $this->getintersection($eventsearly,$people,NULL,SELF::EVENT_TYPE_SHARE_OUTER);
		//	$SHAREOUTER_afterdata_early = $this->caculate($SHAREOUTER_predata_early);

		//	$SHAREgrate_outer=$this->getrate($SHAREOUTER_afterdata,$SHAREOUTER_afterdata_early);
		//	$waibufenxiang = array("waibufenxiang"=>$SHAREgrate_outer);

			array_push($all[$i],$afterdata,$QAINNER_afterdata,$QAOUTER_afterdata,$SHAREINNER_afterdata,$SHAREOUTER_afterdata,$wenzhang);
			
			}

			
//个人数据_TOP
		$all_top = $this->getallpeople();
	for ($i = 0;$i < count($all_top);$i++){
		
		$people = $all_top[$i];

		//文章质量
		//最近的时间
			$articles = $this->getarticles($starttime,$stoptime);
			$predata = $this->getintersection($events,$people,$articles,SELF::EVENT_TYPE_ARTICLE);
			$afterdata = $this->caculate($predata);
			//var_dump($predata);
			//echo"</br>";echo"</br>";
			
		//更早的时间
			$articlesearly = $this->getarticles($datedeal["earlytime"],$datedeal["starttime"]);
			$predataearly = $this->getintersection($eventsearly,$people,$articlesearly,SELF::EVENT_TYPE_ARTICLE);
		    $afterdataearly = $this->caculate($predataearly);
			
			//$articlesgrate=$this->getrate($afterdata,$afterdataearly);
			//$wenzhang = array("wenzhang"=>$articlesgrate);
			array_push($all_top[$i],$predata);
			
			}


	$teamdata = $this->adjustformat($teamarr);
	$persondata_totle = $this->adjustformat_person($all);
	$persondata_top = $this->adjustformat_person_top($all_top);
	$scores = $this->score($teamdata);
//	var_dump($persondata_totle);
	$this->assign("scores", $scores);
	$this->assign("datedeal", $datedeal);
	$this->assign("teamdata", $teamdata);
	$this->assign("persondata", $persondata_totle);
	$this->assign("toprank", $persondata_top);
	
    $res = $this->fetch('tpl.datacenter.htm');
	
	echo $res;
    }
 
}
