<?php
class Page_User_OpaEvents extends Page_OpaBase{
    protected $roomPerPage = 50;

    public function __construct(){
        parent::__construct();
    }
    public function run(){
	$numPerPage = 25;
	
	$page  = isset($this->mGET['page'])? $this->mGET['page'] : '1'; 
	//echo $numPerPage;
	$SqlCommand = SOSO_DB_SQLCommand::getInstance(0);
	$SqlCommand->setCharset("utf8");
	$dataNum = $SqlCommand->ExecuteArrayQuery("SELECT COUNT(*) FROM `Events`")[0]['COUNT(*)'];//计算总量
	$pageNum = ceil($dataNum/$numPerPage);//判断有多少页
	if($page>$pageNum){
		return false;
	}	
	$this->assign("pageNum",$pageNum);//前端循环分页
	$events=$SqlCommand->ExecuteArrayQuery("select * from Events order by EventId desc",$page,$numPerPage,'assoc');
	$eventCate=$SqlCommand->ExecuteArrayQuery("select * from EventCategory");
	$people=$SqlCommand->ExecuteArrayQuery("select * from Person");
	$events=$this->convertflection($events,$eventCate,"EventCategoryId","EventId","EventCategory");
	$events=$this->convertflection($events,$people,"EventPersonId","PersonId","PersonName");
	$events=$this->FillArticleName($events);
	$this->assign("events", $events);
    $res = $this->fetch('tpl.events.htm');
	echo $res;
    }
 
}
