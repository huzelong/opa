<?php
/**
 * @author moonzhang (moonzhang@tencent.com)
 * @version v 1.0 2017-04-19 19:01:12 PM
 * @package Page_Admin
 * 
 */
class Page_Admin_Team extends SOSO_ORM_TableAdmin {
	public function __construct() {
		parent::__construct("Team",0);
	}
	
	public function afterList(&$pList){
		return $pList;	
	}
}
?>
