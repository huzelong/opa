<?php /* Smarty version Smarty-3.1.15, created on 2017-04-28 20:17:37
         compiled from "/search/odin/ziyuan/testsoso/WEB-INF/template/tpl.teamcenter.htm" */ ?>
<?php /*%%SmartyHeaderCode:95632689458f9cab5154262-14197986%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6283b58ce238cfc350987162f490811d781cd78b' => 
    array (
      0 => '/search/odin/ziyuan/testsoso/WEB-INF/template/tpl.teamcenter.htm',
      1 => 1493379946,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '95632689458f9cab5154262-14197986',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.15',
  'unifunc' => 'content_58f9cab5199612_80546479',
  'variables' => 
  array (
    'teamname' => 0,
    'datedeal' => 0,
    'statistic2' => 0,
    'statistic' => 0,
    'articlesgrate' => 0,
    'QAINNER_afterdata_early' => 0,
    'QAINNER_afterdata' => 0,
    'QAgrate_inner' => 0,
    'QAOUTER_afterdata_early' => 0,
    'QAOUTER_afterdata' => 0,
    'QAgrate_outer' => 0,
    'SHAREINNER_afterdata_early' => 0,
    'SHAREINNER_afterdata' => 0,
    'SHAREgrate_inner' => 0,
    'SHAREOUTER_afterdata_early' => 0,
    'SHAREOUTER_afterdata' => 0,
    'SHAREgrate_outer' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58f9cab5199612_80546479')) {function content_58f9cab5199612_80546479($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>公众号文章列表</title>

    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">

    <link href="/script/css/bootstrap.min.css" rel="stylesheet">
    <link href="/script/css/style.css" rel="stylesheet">

  </head>
  <body>

    <div class="container-fluid">
	
		<div class="row">
			
			<div class="col-md-2">
				<div class="row">
				<h3 >
					<span class="glyphicon glyphicon-home"></span>公众号运营系统
				</h3>
				</div>
			
				<div class="row">
				<a href="/opaevents" class="list-group-item">
							事件列表
				</a>
				<a href="/opaarticles" class="list-group-item">文章列表</a>
				<a href="/opateam" class="list-group-item active">团队中心</a>
				<a href="#" class="list-group-item">个人中心</a>
				<a href="/opadatacenter" class="list-group-item">数据中心</a>
				</div>
			</div>
	
	
	
	
		<div class="col-md-10">
		
			<div class="row">
			
				<div class="col-md-12">
					<h3>
						团队:<?php echo $_smarty_tpl->tpl_vars['teamname']->value['TeamName'];?>

					</h3>
				</div>
			</div>
			
			<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								文章数据
							</div>
							
							<div class="panel-body">
								<div class="table-responsive">
									<table class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th>
													开始时间
												</th>
												<th>
													结束时间
												</th>
												<th>
													文章总数
												</th>
												<th>
													阅读量
												</th>
												<th>
													转载量
												</th>
												<th>
													收藏量
												</th>
												<th>
													留言量
												</th>
												<th>
													分享量
												</th>
											</tr>
										</thead>
										<tbody>
										<tr>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['datedeal']->value['earlytime'];?>

											</td>
										
											<td>
											<?php echo $_smarty_tpl->tpl_vars['datedeal']->value['starttime'];?>

											</td>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['statistic2']->value['num'];?>

											</td>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['statistic2']->value['pageviews'];?>

											</td>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['statistic2']->value['reprint'];?>

											</td>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['statistic2']->value['fav'];?>

											</td>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['statistic2']->value['message'];?>

											</td>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['statistic2']->value['share'];?>

											</td>
										</tr>
										
										<tr>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['datedeal']->value['starttime'];?>

											</td>
										
											<td>
											<?php echo $_smarty_tpl->tpl_vars['datedeal']->value['stoptime'];?>

											</td>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['statistic']->value['num'];?>

											</td>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['statistic']->value['pageviews'];?>

											</td>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['statistic']->value['reprint'];?>

											</td>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['statistic']->value['fav'];?>

											</td>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['statistic']->value['message'];?>

											</td>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['statistic']->value['share'];?>

											</td>
										</tr>
										<tr>
											<td>
											变化量
											</td>
										
											<td>
											<?php echo $_smarty_tpl->tpl_vars['datedeal']->value['interval'];?>
天
											</td>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['articlesgrate']->value['num'];?>

											</td>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['articlesgrate']->value['pageviews'];?>

											</td>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['articlesgrate']->value['reprint'];?>

											</td>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['articlesgrate']->value['fav'];?>

											</td>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['articlesgrate']->value['message'];?>

											</td>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['articlesgrate']->value['share'];?>

											</td>
										</tr>
										</tbody>
								</table>
								</div>
							</div>
						</div>
					</div>
			</div>
			<!--答疑-->
			<div class="row">
									<div class="col-md-6">
										<div class="panel panel-default">
											<div class="panel-heading">
												粉丝答疑
											</div>
											
											<div class="panel-body">
												<div class="table-responsive">
													<table class="table table-striped table-bordered table-hover">
														<thead>
															<tr>
																<th>
																	开始时间
																</th>
																<th>
																	结束时间
																</th>
																<th>
																	答疑个数
																</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td>
																<?php echo $_smarty_tpl->tpl_vars['datedeal']->value['earlytime'];?>

																</td>
															
																<td>
																<?php echo $_smarty_tpl->tpl_vars['datedeal']->value['starttime'];?>

																</td>
																<td>
																<?php echo $_smarty_tpl->tpl_vars['QAINNER_afterdata_early']->value['num'];?>

																</td>
															</tr>
															<tr>
																<td>
																<?php echo $_smarty_tpl->tpl_vars['datedeal']->value['starttime'];?>

																</td>
															
																<td>
																<?php echo $_smarty_tpl->tpl_vars['datedeal']->value['stoptime'];?>

																</td>
																<td>
																<?php echo $_smarty_tpl->tpl_vars['QAINNER_afterdata']->value['num'];?>

																</td>
															</tr>
															<tr>
																<td>
																变化量
																</td>
															
																<td>
																<?php echo $_smarty_tpl->tpl_vars['datedeal']->value['interval'];?>
天
																</td>
																<td>
																<?php echo $_smarty_tpl->tpl_vars['QAgrate_inner']->value['num'];?>

																</td>
															</tr>
														</tbody>
												</table>
												</div>
											</div>
										</div>
									</div>
									
									<div class="col-md-6">
										<div class="panel panel-default">
											<div class="panel-heading">
												外部答疑
											</div>
											
											<div class="panel-body">
												<div class="table-responsive">
													<table class="table table-striped table-bordered table-hover">
														<thead>
															<tr>
																<th>
																	开始时间
																</th>
																<th>
																	结束时间
																</th>
																<th>
																	答疑个数
																</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td>
																<?php echo $_smarty_tpl->tpl_vars['datedeal']->value['earlytime'];?>

																</td>
															
																<td>
																<?php echo $_smarty_tpl->tpl_vars['datedeal']->value['starttime'];?>

																</td>
																<td>
																<?php echo $_smarty_tpl->tpl_vars['QAOUTER_afterdata_early']->value['num'];?>

																</td>
															</tr>
															<tr>
																<td>
																<?php echo $_smarty_tpl->tpl_vars['datedeal']->value['starttime'];?>

																</td>
															
																<td>
																<?php echo $_smarty_tpl->tpl_vars['datedeal']->value['stoptime'];?>

																</td>
																<td>
																<?php echo $_smarty_tpl->tpl_vars['QAOUTER_afterdata']->value['num'];?>

																</td>
															</tr>
															<tr>
																<td>
																变化量
																</td>
															
																<td>
																<?php echo $_smarty_tpl->tpl_vars['datedeal']->value['interval'];?>
天
																</td>
																<td>
																<?php echo $_smarty_tpl->tpl_vars['QAgrate_outer']->value['num'];?>

																</td>
															</tr>
														</tbody>
												</table>
												</div>
											</div>
										</div>
									</div>
									
			
			</div>
			<!--分享-->
			<div class="row">
									<div class="col-md-6">
										<div class="panel panel-default">
											<div class="panel-heading">
												公司分享
											</div>
											
											<div class="panel-body">
												<div class="table-responsive">
													<table class="table table-striped table-bordered table-hover">
														<thead>
															<tr>
																<th>
																	开始时间
																</th>
																<th>
																	结束时间
																</th>
																<th>
																	分享次数
																</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td>
																<?php echo $_smarty_tpl->tpl_vars['datedeal']->value['earlytime'];?>

																</td>
															
																<td>
																<?php echo $_smarty_tpl->tpl_vars['datedeal']->value['starttime'];?>

																</td>
																<td>
																<?php echo $_smarty_tpl->tpl_vars['SHAREINNER_afterdata_early']->value['num'];?>

																</td>
															</tr>
															<tr>
																<td>
																<?php echo $_smarty_tpl->tpl_vars['datedeal']->value['starttime'];?>

																</td>
															
																<td>
																<?php echo $_smarty_tpl->tpl_vars['datedeal']->value['stoptime'];?>

																</td>
																<td>
																<?php echo $_smarty_tpl->tpl_vars['SHAREINNER_afterdata']->value['num'];?>

																</td>
															</tr>
															<tr>
																<td>
																变化量
																</td>
															
																<td>
																<?php echo $_smarty_tpl->tpl_vars['datedeal']->value['interval'];?>
天
																</td>
																<td>
																<?php echo $_smarty_tpl->tpl_vars['SHAREgrate_inner']->value['num'];?>

																</td>
															</tr>
														</tbody>
												</table>
												</div>
											</div>
										</div>
									</div>
									
									<div class="col-md-6">
										<div class="panel panel-default">
											<div class="panel-heading">
												外部分享
											</div>
											
											<div class="panel-body">
												<div class="table-responsive">
													<table class="table table-striped table-bordered table-hover">
														<thead>
															<tr>
																<th>
																	开始时间
																</th>
																<th>
																	结束时间
																</th>
																<th>
																	分享个数
																</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td>
																<?php echo $_smarty_tpl->tpl_vars['datedeal']->value['earlytime'];?>

																</td>
															
																<td>
																<?php echo $_smarty_tpl->tpl_vars['datedeal']->value['starttime'];?>

																</td>
																<td>
																<?php echo $_smarty_tpl->tpl_vars['SHAREOUTER_afterdata_early']->value['num'];?>

																</td>
															</tr>
															<tr>
																<td>
																<?php echo $_smarty_tpl->tpl_vars['datedeal']->value['starttime'];?>

																</td>
															
																<td>
																<?php echo $_smarty_tpl->tpl_vars['datedeal']->value['stoptime'];?>

																</td>
																<td>
																<?php echo $_smarty_tpl->tpl_vars['SHAREOUTER_afterdata']->value['num'];?>

																</td>
															</tr>
															<tr>
																<td>
																变化量
																</td>
															
																<td>
																<?php echo $_smarty_tpl->tpl_vars['datedeal']->value['interval'];?>
天
																</td>
																<td>
																<?php echo $_smarty_tpl->tpl_vars['SHAREgrate_outer']->value['num'];?>

																</td>
															</tr>
														</tbody>
												</table>
												</div>
											</div>
										</div>
									</div>
									
			
			</div>
		</div>
		
	</div>
	
	</div>

    <script src="/script/js/jquery.min.js"></script>
    <script src="/script/js/bootstrap.min.js"></script>
    <script src="/script/js/scripts.js"></script>
  </body>
</html>
<?php }} ?>
