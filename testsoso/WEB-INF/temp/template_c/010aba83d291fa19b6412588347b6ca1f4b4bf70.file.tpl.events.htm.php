<?php /* Smarty version Smarty-3.1.15, created on 2017-05-02 17:14:58
         compiled from "/search/odin/ziyuan/testsoso/WEB-INF/template/tpl.events.htm" */ ?>
<?php /*%%SmartyHeaderCode:94475491558f743cb9454b3-07193073%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '010aba83d291fa19b6412588347b6ca1f4b4bf70' => 
    array (
      0 => '/search/odin/ziyuan/testsoso/WEB-INF/template/tpl.events.htm',
      1 => 1493716496,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '94475491558f743cb9454b3-07193073',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.15',
  'unifunc' => 'content_58f743cb97b9b5_06848816',
  'variables' => 
  array (
    'events' => 0,
    'event' => 0,
    'pageNum' => 0,
    'x' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58f743cb97b9b5_06848816')) {function content_58f743cb97b9b5_06848816($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>公众号事件列表</title>

    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">

    <link href="/script/css/bootstrap.min.css" rel="stylesheet">
    <link href="/script/css/style.css" rel="stylesheet">


	
  </head>
  <body>

    <div class="container-fluid">
	
	<div class="row">
		<div class="col-md-2">
		
		<div class="row">
		<h3 >
			<span class="glyphicon glyphicon-home"></span>公众号运营系统
		</h3>
		</div>
		
		<div class="row">
		<a href="/opaevents" class="list-group-item active">
					事件列表
		</a>
		<a href="/opaarticles" class="list-group-item">文章列表</a>
		<a href="/opateam" class="list-group-item">团队中心</a>
		<a href="#" class="list-group-item">个人中心</a>
		<a href="/opadatacenter" class="list-group-item">数据中心</a>
		</div>
		
		</div>
<div class = "col-md-10">
	
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-3">
					<h3>事件列表</h3>
				</div>
				<div class="col-md-3">
				</div>
				<div class="col-md-3">
				</div>
				<div class="col-md-3">
<button class="btn btn-primary pull-right" onclick="window.location.href='/OpaEventInsert'" >新增事件</button>
						</div>
					</div>
				</div>
		
		
	</div>
	<div class="row">
		<div class="col-md-12">
			<table class="table">
				<thead>
					<tr>
						<th>
							EventsId
						</th>
						<th>
							事件名称
						</th>
						<th>
							事件人	
						</th>
						<th>
							事件类型
						</th>
						<th>
                                                        发生时间
                                                </th>
						<th>
                                                        事件描述
                                                </th>
						<th>
                                                        事件地点
                                                </th>						

					</tr>
				</thead>
				<tbody>
				
<?php  $_smarty_tpl->tpl_vars['event'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['event']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['events']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['event']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['event']->key => $_smarty_tpl->tpl_vars['event']->value) {
$_smarty_tpl->tpl_vars['event']->_loop = true;
 $_smarty_tpl->tpl_vars['event']->iteration++;
?>
	 <?php if (!(1 & $_smarty_tpl->tpl_vars['event']->iteration / 1)) {?>
					<tr>
						<td>
							<?php echo $_smarty_tpl->tpl_vars['event']->value['EventId'];?>

						</td>
						<td>
							<?php echo $_smarty_tpl->tpl_vars['event']->value['EventName'];?>

						</td>
						<td>
							<?php echo $_smarty_tpl->tpl_vars['event']->value['EventPersonId'];?>

						</td>
						<td>
							<?php echo $_smarty_tpl->tpl_vars['event']->value['EventCategoryId'];?>

						</td>
						<td>
                                                        <?php echo $_smarty_tpl->tpl_vars['event']->value['EventTime'];?>

                                                </td>
						<td>
                                                        <?php echo $_smarty_tpl->tpl_vars['event']->value['EventDiscription'];?>

                                                </td>
						<td>
                                                        <?php echo $_smarty_tpl->tpl_vars['event']->value['Location'];?>

                                                </td>
					</tr>
<?php } else { ?>
					<tr class="active">
                                                <td>
                                                        <?php echo $_smarty_tpl->tpl_vars['event']->value['EventId'];?>

                                                </td>
                                                <td>
                                                        <?php echo $_smarty_tpl->tpl_vars['event']->value['EventName'];?>

                                                </td>
                                                <td>
                                                        <?php echo $_smarty_tpl->tpl_vars['event']->value['EventPersonId'];?>

                                                </td>
                                                <td>
                                                        <?php echo $_smarty_tpl->tpl_vars['event']->value['EventCategoryId'];?>

                                                </td>
                                                <td>
                                                        <?php echo $_smarty_tpl->tpl_vars['event']->value['EventTime'];?>

                                                </td>
                                                <td>
                                                        <?php echo $_smarty_tpl->tpl_vars['event']->value['EventDiscription'];?>

                                                </td>
                                                <td>
                                                        <?php echo $_smarty_tpl->tpl_vars['event']->value['Location'];?>

                                                </td>

	
					</tr>
 <?php }?>
 <?php } ?>
				</tbody>
			</table>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<ul class="pagination pull-right">
			<?php $_smarty_tpl->tpl_vars['x'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['x']->step = 1;$_smarty_tpl->tpl_vars['x']->total = (int) ceil(($_smarty_tpl->tpl_vars['x']->step > 0 ? $_smarty_tpl->tpl_vars['pageNum']->value+1 - (1) : 1-($_smarty_tpl->tpl_vars['pageNum']->value)+1)/abs($_smarty_tpl->tpl_vars['x']->step));
if ($_smarty_tpl->tpl_vars['x']->total > 0) {
for ($_smarty_tpl->tpl_vars['x']->value = 1, $_smarty_tpl->tpl_vars['x']->iteration = 1;$_smarty_tpl->tpl_vars['x']->iteration <= $_smarty_tpl->tpl_vars['x']->total;$_smarty_tpl->tpl_vars['x']->value += $_smarty_tpl->tpl_vars['x']->step, $_smarty_tpl->tpl_vars['x']->iteration++) {
$_smarty_tpl->tpl_vars['x']->first = $_smarty_tpl->tpl_vars['x']->iteration == 1;$_smarty_tpl->tpl_vars['x']->last = $_smarty_tpl->tpl_vars['x']->iteration == $_smarty_tpl->tpl_vars['x']->total;?>
				<li>
					<a href="?page=<?php echo $_smarty_tpl->tpl_vars['x']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['x']->value;?>
</a>
				</li>
			<?php }} ?>
			</ul>
		</div>
	</div>
	</div>
	</div>
</div>

    <script src="/script/js/jquery.min.js"></script>
    <script src="/script/js/bootstrap.min.js"></script>
    <script src="/script/js/scripts.js"></script>
  </body>
</html>
<?php }} ?>
