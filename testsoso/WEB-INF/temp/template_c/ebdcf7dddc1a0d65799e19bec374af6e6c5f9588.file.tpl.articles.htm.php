<?php /* Smarty version Smarty-3.1.15, created on 2017-04-26 21:36:03
         compiled from "/search/odin/ziyuan/testsoso/WEB-INF/template/tpl.articles.htm" */ ?>
<?php /*%%SmartyHeaderCode:201692690158f95a03ebb390-55490164%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ebdcf7dddc1a0d65799e19bec374af6e6c5f9588' => 
    array (
      0 => '/search/odin/ziyuan/testsoso/WEB-INF/template/tpl.articles.htm',
      1 => 1493213747,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '201692690158f95a03ebb390-55490164',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.15',
  'unifunc' => 'content_58f95a03ef9d52_47800963',
  'variables' => 
  array (
    'articles' => 0,
    'article' => 0,
    'pageNum' => 0,
    'x' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58f95a03ef9d52_47800963')) {function content_58f95a03ef9d52_47800963($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>公众号文章列表</title>

    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">

    <link href="/script/css/bootstrap.min.css" rel="stylesheet">
    <link href="/script/css/style.css" rel="stylesheet">

  </head>
  <body>

    <div class="container-fluid">
	
		<div class="row">
		<div class="col-md-2">
		
		<div class="row">
		<h3 >
			<span class="glyphicon glyphicon-home"></span>公众号运营系统
		</h3>
		</div>
		
		<div class="row">
		<a href="/opaevents" class="list-group-item">
					事件列表
		</a>
		<a href="/opaarticles" class="list-group-item active">文章列表</a>
		<a href="/opateam" class="list-group-item">团队中心</a>
		<a href="#" class="list-group-item">个人中心</a>
		<a href="/opadatacenter" class="list-group-item">数据中心</a>
		</div>
		
		</div>
	
	
	
	
	<div class="col-md-10">
	
	<div class="row">
		<div class="col-md-12">
			<h3>
				公众号文章列表
			</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<table class="table">
				<thead>
					<tr>
						<th>
							标题
						</th>
						<th>
							笔名	
						</th>
						<th>
							阅读量
						</th>
						<th>
							转载量
                        </th>
						<th>
							分享量
						</th>
						<th>
                        收藏量
                        </th>
						<th>
                         留言量
                        </th>
				         <th>
						发表时间
						</th>	
					</tr>
				</thead>
				<tbody>
				
<?php  $_smarty_tpl->tpl_vars['article'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['article']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['articles']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['article']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['article']->key => $_smarty_tpl->tpl_vars['article']->value) {
$_smarty_tpl->tpl_vars['article']->_loop = true;
 $_smarty_tpl->tpl_vars['article']->iteration++;
?>
	 <?php if (!(1 & $_smarty_tpl->tpl_vars['article']->iteration / 1)) {?>
					<tr>
        <td><?php echo $_smarty_tpl->tpl_vars['article']->value['ArticleTitle'];?>
</td>
        <td><?php echo $_smarty_tpl->tpl_vars['article']->value['NetName'];?>
</td>
        <td><?php echo $_smarty_tpl->tpl_vars['article']->value['PageViews'];?>
</td>
        <td><?php echo $_smarty_tpl->tpl_vars['article']->value['Reprint'];?>
</td>
        <td><?php echo $_smarty_tpl->tpl_vars['article']->value['Share'];?>
</td>
	<td><?php echo $_smarty_tpl->tpl_vars['article']->value['Fav'];?>
</td>
        <td><?php echo $_smarty_tpl->tpl_vars['article']->value['Message'];?>
</td>
	<td><?php echo $_smarty_tpl->tpl_vars['article']->value['LauchDate'];?>
</td>
					</tr>
<?php } else { ?>
					<tr class="active">
        <td><?php echo $_smarty_tpl->tpl_vars['article']->value['ArticleTitle'];?>
</td>
        <td><?php echo $_smarty_tpl->tpl_vars['article']->value['NetName'];?>
</td>
        <td><?php echo $_smarty_tpl->tpl_vars['article']->value['PageViews'];?>
</td>
        <td><?php echo $_smarty_tpl->tpl_vars['article']->value['Reprint'];?>
</td>
        <td><?php echo $_smarty_tpl->tpl_vars['article']->value['Share'];?>
</td>
        <td><?php echo $_smarty_tpl->tpl_vars['article']->value['Fav'];?>
</td>
        <td><?php echo $_smarty_tpl->tpl_vars['article']->value['Message'];?>
</td>
        <td><?php echo $_smarty_tpl->tpl_vars['article']->value['LauchDate'];?>
</td>		
						</tr>
 <?php }?>
 <?php } ?>
				</tbody>
			</table>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<ul class="pagination pull-right">
			<?php $_smarty_tpl->tpl_vars['x'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['x']->step = 1;$_smarty_tpl->tpl_vars['x']->total = (int) ceil(($_smarty_tpl->tpl_vars['x']->step > 0 ? $_smarty_tpl->tpl_vars['pageNum']->value+1 - (1) : 1-($_smarty_tpl->tpl_vars['pageNum']->value)+1)/abs($_smarty_tpl->tpl_vars['x']->step));
if ($_smarty_tpl->tpl_vars['x']->total > 0) {
for ($_smarty_tpl->tpl_vars['x']->value = 1, $_smarty_tpl->tpl_vars['x']->iteration = 1;$_smarty_tpl->tpl_vars['x']->iteration <= $_smarty_tpl->tpl_vars['x']->total;$_smarty_tpl->tpl_vars['x']->value += $_smarty_tpl->tpl_vars['x']->step, $_smarty_tpl->tpl_vars['x']->iteration++) {
$_smarty_tpl->tpl_vars['x']->first = $_smarty_tpl->tpl_vars['x']->iteration == 1;$_smarty_tpl->tpl_vars['x']->last = $_smarty_tpl->tpl_vars['x']->iteration == $_smarty_tpl->tpl_vars['x']->total;?>
				<li>
					<a href="?page=<?php echo $_smarty_tpl->tpl_vars['x']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['x']->value;?>
</a>
				</li>
			<?php }} ?>
			</ul>
		</div>
	</div>
</div>
</div>
</div>

    <script src="/script/js/jquery.min.js"></script>
    <script src="/script/js/bootstrap.min.js"></script>
    <script src="/script/js/scripts.js"></script>
  </body>
</html>
<?php }} ?>
