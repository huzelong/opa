<?php /* Smarty version Smarty-3.1.15, created on 2017-05-05 15:03:40
         compiled from "/search/odin/ziyuan/testsoso/WEB-INF/template/tpl.datacenter.htm" */ ?>
<?php /*%%SmartyHeaderCode:179278247058ff11309ace94-10746043%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0d5189d8a872ddda3aa552dba9e8a5ff42557b88' => 
    array (
      0 => '/search/odin/ziyuan/testsoso/WEB-INF/template/tpl.datacenter.htm',
      1 => 1493967810,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '179278247058ff11309ace94-10746043',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.15',
  'unifunc' => 'content_58ff1130a34250_85716620',
  'variables' => 
  array (
    'datedeal' => 0,
    'scores' => 0,
    'score' => 0,
    'teamdata' => 0,
    'team' => 0,
    'persondata' => 0,
    'person' => 0,
    'toprank' => 0,
    'top' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58ff1130a34250_85716620')) {function content_58ff1130a34250_85716620($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>公众号文章列表</title>

    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">

    <link href="/script/css/bootstrap.min.css" rel="stylesheet">
    <link href="/script/css/style.css" rel="stylesheet">

  </head>
  <body>

    <div class="container-fluid">
	
		<div class="row">
			
			<div class="col-md-2">
				<div class="row">
				<h3 >
					<span class="glyphicon glyphicon-home"></span>公众号运营系统
				</h3>
				</div>
			
				<div class="row">
				<a href="/opaevents" class="list-group-item">
							事件列表
				</a>
				<a href="/opaarticles" class="list-group-item">文章列表</a>
				<a href="/opateam" class="list-group-item">团队中心</a>
				<a href="#" class="list-group-item">个人中心</a>
				<a href="/opadatacenter" class="list-group-item active">数据中心</a>
				</div>
			</div>
	
	
	
	
		<div class="col-md-10">
		
			<div class="row">
			
				<div class="col-md-12">
					<h3>
						数据中心
					</h3>
				</div>
			</div>
			
			
		<div class="row">
		
		<form class="form-horizontal" id = "form_data" role="form" method="post" onsubmit="return check_form_teamcenter()" action="/opadatacenter">
			<div class="col-md-3">
					<!--bootstrap-datepicker-->
					<div class="form-group">
						<label for="starttime" class="col-sm-4 control-label ">统计开始时间:</label>
						<div class="input-group date form_date col-sm-8" data-date="" data-date-format="yyyy mm dd" data-link-field="starttime" data-link-format="yyyy-mm-dd">
							<input class="form-control" type="text" value="<?php echo $_smarty_tpl->tpl_vars['datedeal']->value['starttime'];?>
" readonly name="starttime" id="starttime">
							<span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
							<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
						</div>
						<input type="hidden" id="starttime" value="" /><br/>
					</div>
				</div>
				<div class="col-md-3">
					<!--bootstrap-datepicker-->
					<div class="form-group">
						<label for="stoptime" class="col-sm-4 control-label pull-left">统计结束时间:</label>
						<div class="input-group date form_date col-sm-8" data-date="" data-date-format="yyyy mm dd" data-link-field="stoptime" data-link-format="yyyy-mm-dd">
							<input class="form-control" type="text" value="<?php echo $_smarty_tpl->tpl_vars['datedeal']->value['stoptime'];?>
" readonly name="stoptime" id="stoptime">
							<span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
							<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
						</div>
						<input type="hidden" id="stoptime" value="" /><br/>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<div class="col-sm-offset-1 col-sm-10">
							<button type="submit" class="btn btn-primary pull-left">
								提交
							</button>
						</div>
					</div>
				</div>
			</form>
			
			<div class="col-md-3">
					<label for="stoptime" class="col-sm-4 control-label pull-left"><?php echo $_smarty_tpl->tpl_vars['datedeal']->value['interval'];?>
天</label>
								
			</div>
				
				
			</div>
			
			
			
			
			
			<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								团队评分
							</div>
							
							<div class="panel-body">
								<div class="table-responsive">
									<table class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th>
													团队
												</th>
												<th>
													阅读增量得分
												</th>
												<th>
													转载增量得分
												</th>
												<th>
													收藏增量得分
												</th>
												<th>
													留言增量得分
												</th>
												<th>
													分享增量得分
												</th>
												<th>
													粉丝答疑得分
												</th>
												<th>
													外部答疑得分
												</th>
												<th>
													内部分享得分
												</th>
												<th>
													外部分享得分
												</th>
												<th>
													总分
												</th>
											</tr>
										</thead>
										<tbody>
										
					<?php  $_smarty_tpl->tpl_vars['score'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['score']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['scores']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['score']->key => $_smarty_tpl->tpl_vars['score']->value) {
$_smarty_tpl->tpl_vars['score']->_loop = true;
?>
					<tr>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['score']->value['teamname'];?>

											</td>
										
											<td>
											<?php echo $_smarty_tpl->tpl_vars['score']->value['pageviews'];?>

											</td>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['score']->value['reprint'];?>

											</td>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['score']->value['fav'];?>

											</td>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['score']->value['message'];?>

											</td>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['score']->value['share'];?>

											</td>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['score']->value['neibudayi'];?>

											</td>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['score']->value['waibudayi'];?>

											</td>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['score']->value['neibufenxiang'];?>

											</td>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['score']->value['waibufenxiang'];?>

											</td>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['score']->value['total'];?>

											</td>
											
							
										</tr>
										<?php } ?>	
										</tbody>
								</table>
								</div>
							</div>
						</div>
					</div>
			</div>
<!--团队具体数据-->
			<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								团队数据
							</div>
							
							<div class="panel-body">
								<div class="table-responsive">
									<table class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th>
													团队
												</th>
												<th>
													阅读增量
												</th>
												<!--th>
													转载增量
												</th-->
												<th>
													收藏增量
												</th>
												<th>
													留言增量
												</th>
												<th>
													分享增量
												</th>
												<th>
													粉丝答疑量
												</th>
												<th>
													外部答疑量
												</th>
												<th>
													内部分享量
												</th>
												<th>
													外部分享量
												</th>
												
											</tr>
										</thead>
										<tbody>
										
							<?php  $_smarty_tpl->tpl_vars['team'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['team']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['teamdata']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['team']->key => $_smarty_tpl->tpl_vars['team']->value) {
$_smarty_tpl->tpl_vars['team']->_loop = true;
?>
									<tr>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['team']->value['teamname'];?>

											</td>
										
											<td>
											<?php echo $_smarty_tpl->tpl_vars['team']->value['pageviews'];?>

											</td>
											<!--th>
													转载增量
												</th-->
											<td>
											<?php echo $_smarty_tpl->tpl_vars['team']->value['fav'];?>

											</td>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['team']->value['message'];?>

											</td>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['team']->value['share'];?>

											</td>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['team']->value['neibudayi'];?>

											</td>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['team']->value['waibudayi'];?>

											</td>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['team']->value['neibufenxiang'];?>

											</td>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['team']->value['waibufenxiang'];?>

											</td>
											
									</tr>
							<?php } ?>	
									
									
								
										</tbody>
								</table>
								</div>
							</div>
						</div>
					</div>
			</div>


<!--个人数据总量-->

			<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								个人数据-总量
							</div>
							
							<div class="panel-body">
								<div class="table-responsive">
									<table class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th>
													名字
												</th>
												<th>
													上次发文总量
												</th>
												<th>
													上次阅读总量
												</th>
												<th>
													本次发文总量
												</th>
												<th>
													本次阅读总量
												</th>
												<th>
													文章平均增量
												</th>
												<th>
													总阅读增量
												</th>
											
												<th>
													文章收藏量
												</th>
												<th>
													文章留言量
												</th>
												<th>
													文章分享量
												</th>
												<th>
													粉丝答疑量
												</th>
												<th>
													外部答疑量
												</th>
												<th>
													内部分享量
												</th>
												<th>
													外部分享量
												</th>
												
											</tr>
										</thead>
										<tbody>
								<?php  $_smarty_tpl->tpl_vars['person'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['person']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['persondata']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['person']->key => $_smarty_tpl->tpl_vars['person']->value) {
$_smarty_tpl->tpl_vars['person']->_loop = true;
?>
										<tr>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['person']->value['personename'];?>

											</td>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['person']->value['lastArticleNum'];?>

											</td>
										
											<td>
											<?php echo $_smarty_tpl->tpl_vars['person']->value['lastArticlePageview'];?>

											</td>
											
											
											<td>
											<?php echo $_smarty_tpl->tpl_vars['person']->value['pagenum'];?>

											</td>
										
											<td>
											<?php echo $_smarty_tpl->tpl_vars['person']->value['pageviews'];?>

											</td>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['person']->value['singleArticleGrate'];?>

											</td>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['person']->value['wenzhangzengliang'];?>

											</td>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['person']->value['fav'];?>

											</td>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['person']->value['message'];?>

											</td>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['person']->value['share'];?>

											</td>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['person']->value['neibudayi'];?>

											</td>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['person']->value['waibudayi'];?>

											</td>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['person']->value['neibufenxiang'];?>

											</td>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['person']->value['waibufenxiang'];?>

											</td>
										</tr>
									<?php } ?>
										</tbody>
								</table>
								</div>
							</div>
						</div>
					</div>
			</div>

<!--个人数据文章TOP-->

			<div class="row">
					<div class="col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								个人数据-TOP
							</div>
							
							<div class="panel-body">
								<div class="table-responsive">
									<table class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th>
													名字
												</th>
												<th>
													文章名字
												</th>
												<th>
													笔名
												</th>
												<th>
													浏览量
												</th>
												<th>
													分享量
												</th>
											
												<th>
													转载量
												</th>
												<th>
													收藏量
												</th>
												<th>
													留言量
												</th>
												<th>
													发布时间
												</th>

											</tr>
										</thead>
										<tbody>
								<?php  $_smarty_tpl->tpl_vars['top'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['top']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['toprank']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['top']->key => $_smarty_tpl->tpl_vars['top']->value) {
$_smarty_tpl->tpl_vars['top']->_loop = true;
?>
										<tr>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['top']->value['personname'];?>

											</td>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['top']->value['ArticleTitle'];?>

											</td>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['top']->value['NetName'];?>

											</td>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['top']->value['PageViews'];?>

											</td>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['top']->value['Share'];?>

											</td>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['top']->value['Reprint'];?>

											</td>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['top']->value['Fav'];?>

											</td>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['top']->value['Message'];?>

											</td>
											<td>
											<?php echo $_smarty_tpl->tpl_vars['top']->value['LauchDate'];?>

											</td>
										</tr>
									<?php } ?>
										</tbody>
								</table>
								</div>
							</div>
						</div>
					</div>
			</div>







									
			
			</div>
		</div>
		
	</div>
	

<script src="/script/js/jquery.min.js"></script>
<script src="/script/js/bootstrap.min.js"></script>
<script src="/script/js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script src="/script/js/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.zh-CN.js"></script>
<script src="/script/js/scripts.js"></script>
	<script>
	$('.form_date').datetimepicker({
	  language:  'zh-CN',
	  weekStart: 1,
	  todayBtn:  1,
	  autoclose: 1,
	  todayHighlight: 1,
	  startView: 2,
	  minView: 2,
	  forceParse: 0
	  });
	</script>
  </body>
</html>
<?php }} ?>
