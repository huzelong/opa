<?php /* Smarty version Smarty-3.1.15, created on 2017-04-26 15:44:38
         compiled from "/search/odin/ziyuan/testsoso/WEB-INF/template/tpl.eventInsert.htm" */ ?>
<?php /*%%SmartyHeaderCode:177258148658f96ec7ed57e0-81522451%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '88434db4fbd88decf201228f88e2986f4a2077de' => 
    array (
      0 => '/search/odin/ziyuan/testsoso/WEB-INF/template/tpl.eventInsert.htm',
      1 => 1493110519,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '177258148658f96ec7ed57e0-81522451',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.15',
  'unifunc' => 'content_58f96ec7ef17a7_68366543',
  'variables' => 
  array (
    'eventCate' => 0,
    'ecate' => 0,
    'people' => 0,
    'person' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58f96ec7ef17a7_68366543')) {function content_58f96ec7ef17a7_68366543($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>新建公众号事件</title>

<meta name="description" content="Source code generated using layoutit.com">
<meta name="author" content="LayoutIt!">
<link href="/script/css/bootstrap.min.css" rel="stylesheet"> 
<link href="/script/css/style.css" rel="stylesheet">    
<link href="/script/js/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
</head>
<body>

<div class="container-fluid">

		<div class="row">
		<div class="col-md-2">
		
		<div class="row">
		<h3 >
			<span class="glyphicon glyphicon-home"></span>公众号运营系统
		</h3>
		</div>
		
		<div class="row">
		<a href="/opaevents" class="list-group-item active">
					事件列表
		</a>
		<a href="/opaarticles" class="list-group-item ">文章列表</a>
		<a href="/opateam" class="list-group-item">团队中心</a>
		<a href="#" class="list-group-item">个人中心</a>
		<a href="/opadatacenter" class="list-group-item">数据中心</a>
		</div>
		
		</div>


<div class="col-md-10">
	<div class="row">	
		<div class="col-md-12">
			<h3>
			新建事件
			</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="row">
			<div class="col-md-4">
			</div>
			<div class="col-md-4">

			<div class="row">
				<form class="form-horizontal" id = "form_data" role="form" method="post" onsubmit="return check_form()">
					<div class="form-group">
						<label for="ecate" class="col-sm-4 control-label">
								事件类型
						</label>
						
						<div class="col-sm-8">
								<select class="form-control" id="ecate" name = "ecate">
							   <?php  $_smarty_tpl->tpl_vars['ecate'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['ecate']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['eventCate']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['ecate']->key => $_smarty_tpl->tpl_vars['ecate']->value) {
$_smarty_tpl->tpl_vars['ecate']->_loop = true;
?>
									<option id= "<?php echo $_smarty_tpl->tpl_vars['ecate']->value['EventId'];?>
"><?php echo $_smarty_tpl->tpl_vars['ecate']->value['EventCategory'];?>
</option>
								<?php } ?>
								</select>                                        
						</div>
					</div>
				
					<div class="form-group" style="display:none" id="enamediv">
						<label for="ename" class="col-sm-4 control-label">
									事件名称
						</label>
						<div class="col-sm-8">
							<input type="input" class="form-control" id="ename" name="ename"/>
						</div>
					</div>
					<div class="form-group" id="epersondiv">

							<label for="eperson" class="col-sm-4 control-label">
									事件人
							</label> 
							<div class="col-sm-8">
									<select class="form-control" id="eperson" name ="eperson">
									<?php  $_smarty_tpl->tpl_vars['person'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['person']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['people']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['person']->key => $_smarty_tpl->tpl_vars['person']->value) {
$_smarty_tpl->tpl_vars['person']->_loop = true;
?>
									<option id= "<?php echo $_smarty_tpl->tpl_vars['person']->value['PersonId'];?>
"><?php echo $_smarty_tpl->tpl_vars['person']->value['PersonName'];?>
</option>
									<?php } ?>
									</select>
							</div>
					</div>

					<!--bootstrap-datepicker-->
					<div class="form-group">
						<label for="etime" class="col-sm-4 control-label">发生时间</label>
						<div class="input-group date form_date col-sm-8" data-date="" data-date-format="dd MM yyyy" data-link-field="etime" data-link-format="yyyy-mm-dd">
							<input class="form-control" type="text" value="" readonly name="etime">
							<span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
							<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
						</div>
						<input type="hidden" id="etime" value="" /><br/>
					</div>

					<div class="form-group" style="display:none" id ="discribe"> 
							<label for="ediscribe" class="col-sm-4 control-label">
									事件描述
							</label>
							<div class="col-sm-8">
									<input type="text" class="form-control" id="ediscribe" name="ediscribe" />
							</div>
					</div>

					<div class="form-group" style="display:none" id = "location">
							<label for="elocation" class="col-sm-4 control-label">
									事件地点
							</label>
							<div class="col-sm-8">
									<input type="text" class="form-control" id="elocation" name="elocation"/>
							</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							 
							<button type="submit" class="btn btn-primary pull-right">
								提交
							</button>
						</div>
					</div>
			</form>
			</div>
			</div>
			<div class="col-md-4">
			</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>

<script src="/script/js/jquery.min.js"></script>
<script src="/script/js/bootstrap.min.js"></script>
<script src="/script/js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script src="/script/js/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.zh-CN.js"></script>
<script src="/script/js/scripts.js"></script>
<script>
$('.form_date').datetimepicker({
  language:  'zh-CN',
  weekStart: 1,
  todayBtn:  1,
  autoclose: 1,
  todayHighlight: 1,
  startView: 2,
  minView: 2,
  forceParse: 0
  });
</script>

</body>                               
</html>     

<?php }} ?>
