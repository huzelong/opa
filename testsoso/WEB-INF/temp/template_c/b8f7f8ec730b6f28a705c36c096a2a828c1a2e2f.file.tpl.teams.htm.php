<?php /* Smarty version Smarty-3.1.15, created on 2017-04-25 16:53:58
         compiled from "/search/odin/ziyuan/testsoso/WEB-INF/template/tpl.teams.htm" */ ?>
<?php /*%%SmartyHeaderCode:122876869958f9b1e79f7094-12760043%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b8f7f8ec730b6f28a705c36c096a2a828c1a2e2f' => 
    array (
      0 => '/search/odin/ziyuan/testsoso/WEB-INF/template/tpl.teams.htm',
      1 => 1493110434,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '122876869958f9b1e79f7094-12760043',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.15',
  'unifunc' => 'content_58f9b1e7a1a6b8_27776364',
  'variables' => 
  array (
    'teams' => 0,
    'team' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_58f9b1e7a1a6b8_27776364')) {function content_58f9b1e7a1a6b8_27776364($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>公众号文章列表</title>

    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">

    <link href="/script/css/bootstrap.min.css" rel="stylesheet">
    <link href="/script/css/style.css" rel="stylesheet">

  </head>
  <body>

    <div class="container-fluid">
	
		<div class="row">
		<div class="col-md-2">
		
		<div class="row">
		<h3 >
			<span class="glyphicon glyphicon-home"></span>公众号运营系统
		</h3>
		</div>
		
		<div class="row">
		<a href="/opaevents" class="list-group-item">
					事件列表
		</a>
		<a href="/opaarticles" class="list-group-item">文章列表</a>
		<a href="/opateam" class="list-group-item active">团队中心</a>
		<a href="#" class="list-group-item">个人中心</a>
		<a href="/opadatacenter" class="list-group-item">数据中心</a>
		</div>
		
		</div>
	
	
	
	
	<div class="col-md-10">
	
	<div class="row">
		<div class="col-md-12">
			<h3>
				团队中心
			</h3>
		</div>
	</div>

	<div class="row">
		<div class="col-md-4">
		</div>

				<div class="col-md-4">
				<form class="form-horizontal" id = "form_data" role="form" method="post" onsubmit="return check_form_teamcenter()" action="/opateamcenter">
					<div class="form-group">
						<label for="ecate" class="col-md-4 control-label" >
								团队名称:
						</label>
						
						<div class="col-md-8">
								<select class="form-control" id="teamid" name = "teamid" >
							   <?php  $_smarty_tpl->tpl_vars['team'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['team']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['teams']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['team']->key => $_smarty_tpl->tpl_vars['team']->value) {
$_smarty_tpl->tpl_vars['team']->_loop = true;
?>
									<option value= <?php echo $_smarty_tpl->tpl_vars['team']->value['TeamId'];?>
><?php echo $_smarty_tpl->tpl_vars['team']->value['TeamName'];?>
</option>
								<?php } ?>
								</select>                                        
						</div>
					</div>
				

					<!--bootstrap-datepicker-->
					<div class="form-group">
						<label for="starttime" class="col-sm-4 control-label">统计开始时间:</label>
						<div class="input-group date form_date col-sm-8" data-date="" data-date-format="yyyy mm dd" data-link-field="starttime" data-link-format="yyyy-mm-dd">
							<input class="form-control" type="text" value="" readonly name="starttime" id="starttime">
							<span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
							<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
						</div>
						<input type="hidden" id="starttime" value="" /><br/>
					</div>
					<!--bootstrap-datepicker-->
					<div class="form-group">
						<label for="stoptime" class="col-sm-4 control-label">统计结束时间:</label>
						<div class="input-group date form_date col-sm-8" data-date="" data-date-format="yyyy mm dd" data-link-field="stoptime" data-link-format="yyyy-mm-dd">
							<input class="form-control" type="text" value="" readonly name="stoptime" id="stoptime">
							<span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
							<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
						</div>
						<input type="hidden" id="stoptime" value="" /><br/>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							 
							<button type="submit" class="btn btn-primary pull-right">
								提交
							</button>
						</div>
					</div>
			</form>
			</div>
			</div>
			<div class="col-md-4">
			</div>
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	</div>
</div>
</div>

<script src="/script/js/jquery.min.js"></script>
<script src="/script/js/bootstrap.min.js"></script>
<script src="/script/js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script src="/script/js/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.zh-CN.js"></script>
<script src="/script/js/scripts.js"></script>
	<script>
	$('.form_date').datetimepicker({
	  language:  'zh-CN',
	  weekStart: 1,
	  todayBtn:  1,
	  autoclose: 1,
	  todayHighlight: 1,
	  startView: 2,
	  minView: 2,
	  forceParse: 0
	  });
	</script>
  </body>
</html>
<?php }} ?>
