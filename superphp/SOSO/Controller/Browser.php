<?php

/**
 * SOSO Framework
 * @category   SOSO
 * @package    SOSO_Controller
 * @copyright  Copyright (c) 2007-2008 Soso.com
 * @author moonzhang
 * @version 1.0
 * @created 15-四月-2008 16:59:19
 */
//require_once(dirname(dirname(__FILE__)) . "/Interface/Runnable.php");
$dir = dirname(dirname(__FILE__));
require_once($dir . "/View/Page.php");

class SOSO_Controller_Browser /*extends SOSO_Object*/ implements SOSO_Controller_Abstract {

    public $mName = 'front';

    /**
     * 控制器入口
     * 
     * @return void
     */
    public function dispatch($pClass=null) {
        $class = strlen($pClass) ? $pClass : $this->getClass();
        try{
        	$page = new $class();
        	$app = SOSO_Application::getInstance();
        	$action = $app->getAction();
        	$args = $app->getArgs();
        	
        	if($page instanceof SOSO_Base_Util_Observable){
	        	if($page->fireEvent('beforeTemplateInit',$action,$args,$page) === false){
		        	if(SOSO_Frameworks_Context::getInstance()->get('debug')){
		        		SOSO_Debugger::instance()->log("Page Init return false;","PageEvent");
					}
					return false;
	        	}
	        	
	        	
	        	$tEvent = sprintf("before".str_replace('_', '', $action));
	        	if(false === $page->fireEvent($tEvent,$args,$page)){
	        		if(SOSO_Frameworks_Context::getInstance()->get('debug')){
	        			SOSO_Debugger::instance()->log("$tEvent return false;","PageEvent");
	        		}
	        		return false;
	        	}
        	}
        	$ret = call_user_func_array(array($page,$action), $args);
        	if($page instanceof SOSO_Base_Util_Observable){
	        	$page->fireEvent('pageLoaded',$action,$args,$page);
        	}
        }catch(Exception $error){
        	if(SOSO_Frameworks_Config::getMode() == 'debug'){
        		echo $error->getMessage();
        	}else{
        		$oPage = new SOSO_View_Page();
        		$oPage->showMessage($error->getMessage());
        	}
        }
    }

    /**
     * 得到页面类相关信息：类名，filters,action
     * 
     * @return string 返回请求的页面类名
     */
    public function getClass() {
        $context = SOSO_Frameworks_Context::getInstance();
        return $context->getRequest();
    }

}
