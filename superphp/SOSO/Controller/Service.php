<?php
/**
 * SOSO Framework
 * @category   SOSO
 * @package    SOSO_Controller
 * @copyright  Copyright (c) 2007-2008 Soso.com
 * @author moonzhang
 * @version 1.0
 * @created 15-四月-2008 16:59:20
 */
class SOSO_Controller_Service /*extends SOSO_Object*/  implements SOSO_Controller_Abstract {

	/**
	 * @param string $page 页面类
	 * 
	 */
	public function dispatch($pClass=null){
		$class = strlen($pClass) ? $pClass : SOSO_Frameworks_Context::getInstance()->getRequest();
		if (!class_exists($class)) {
			throw new Exception('页面类不存在',1025);
		}
		
		$tShow = SOSO_Frameworks_Config::getMode() != 'online';
		$service = new $class;
		if(!($service instanceof SOSO_Interface_Service) || $service->canAccess() !== true){
			if($tShow){
				exit("Access denied!");
			}
		}
		$server = new SOSO_Proxy_Server($service,array(
		   'displayInfo' => $tShow, 
		   'ignoreOutput'=> !$tShow
		 ));
		$server->displayInfo = $tShow;
		$server->service();
	}

}
?>
