<?php
/**
 * @author moonzhang
 * @version 1.0
 * @2017-03-13 12:34PM
 * 完全模拟XMLReader实现
 */
class SOSO_Base_Data_JSONReader2 extends SOSO_Base_Data_Reader {
    private $mNeedConvert = false;
    
    function __construct($meta,$recordType=null) {
        if (is_null($recordType) && isset($meta['fields'])){
            $recordType = $meta['fields'];
        }
        parent::__construct($meta,$recordType);
    }

    function __destruct() {
    }

    /**
     *
     * @param response
     */
    public function read($json,$store=null,$encoding=null) {
        if($encoding){
            if($encoding != $this->outputEncoding){
                $this->mNeedConvert=true;//需要编码转换
                $this->outputEncoding = $encoding ;
            }
        }

        $j = $json;
        if(!is_array($json)){
            $j = json_decode($json,true);
        }

        if(!$j){
            throw new Exception("JsonReader.read: json string not available",332211);
        }
        //$this->xmlData = $dom;
        return $this->readRecords($j,$store);
    }

    protected function convert($res,$fromCharset,$toCharset){
        if(is_numeric($res)){
            return $res;
        }

        if(is_string($res)){
            if (function_exists('mb_convert_encoding')) {
                $res = mb_convert_encoding($res,$toCharset,$fromCharset);
            }
            else{
                $res = iconv($fromCharset,$toCharset,$res);
            }
        }else{
            foreach ($res as $key=>$val) {
                $res[$key] = $this->convert($val, $fromCharset,$toCharset);
            }
        }

        return $res;
    }
    /**
     *
     * @param param
     */
    public function readRecords($jsobj,$store=null) {
        $root = $jsobj;
        $recordType = $this->recordType;
        $fields = $recordType->fields;
        $sid = isset($this->meta['id']) ? $this->meta['id'] : null;
        $totalRecords = 0;
        $success = true;

        if (isset($this->meta['totalRecords'])){
            $totalRecords = SOSO_Util_Hash::get($root,$this->meta['totalRecords']);
        }

        $records = array();

        $meta = str_replace('*','{s}',trim($this->meta['record']));
        if(strlen($meta) && substr($meta,0,-3) != '{n}'){
            $meta .= '.{n}';
        }
        $meta = preg_replace('#([^\s])\s+([^\s])#Us','$1.$2',$meta);

        $ns = SOSO_Util_Hash::extract($root,$meta);
        $len=count($ns);

        for($i=0; $i<$len; $i++){
            $node = $ns[$i];
            $values = array();
            $id = null;
            if(!is_null($sid)){
                $id = SOSO_Util_Hash::get($node,$sid);
            }

            for($j=0,$jlen=$fields->length;$j<$jlen;$j++){
                $field = $fields->items[$j];
                if (isset($field->mapping)){
                    $sel = $field->mapping;
                    $sel = preg_replace('#([^\s])\s+([^\s])#Us','.',$sel);
                    $v = SOSO_Util_Hash::get($node,"$sel",$field->defaultValue);
                }else{
                    $name = $sel = preg_replace('#([^\s])\s+([^\s])#Us','.',$field->name);
                    $v = $v = SOSO_Util_Hash::get($node,$field->name,$field->defaultValue);
                }

                if (is_null($v)) {
                    $v = $field->defaultValue;
                }

                if ($this->mNeedConvert){
                    $v = $this->convert($v,$this->inputEncoding,$this->outputEncoding);
                }
                $values[$field->name] = $v;
            }

            $record = $recordType->instance($values,$id);
            $record->node = $node;
            $records[] = $record;
        }

        return array('success'=>$success,'records'=>$records,'totalRecords'=>$totalRecords?$totalRecords:count($records));
    }

}
?>
