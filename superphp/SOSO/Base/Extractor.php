<?php
/**
 * @author moonzhang
 * @version 0.0.1 2012-06-26
 * 
 */
class SOSO_Base_Extractor {
	protected $url = '';
	/**
	 *
	 * Enter description here ...
	 * @var SOSO_Base_Data_Store
	 */
	protected $store;
	
	protected $initialed = false;
	/**
	 *
	 * Enter description here ...
	 * @var SOSO_Base_Data_Reader
	 */
	protected $reader;

	protected $meta = array('record'=>'body');
	protected $columns = array();

	public function __construct($url=''){
		if($url) $this->url = $url;
		$this->init($this->meta, $this->columns);
	}
	
	public function getStore(){
		return $this->store;
	}
	
	public function getReader(){
		return $this->reader;
	}
	
	public function getData(){
		return $this->store->getData();
	}
	
	public function getConnection(){
		return $this->store->getProxy();	
	}
	
	public function load($op=array()){
		return $this->store->load($op);
	}

	public function init($meta,$columns){
		if(!$this->url || $this->url == $this->initialed) return false;
		
		$this->meta = $meta;
		$this->columns = $columns;
		
		$this->reader = $reader = new SOSO_Base_Data_XMLReader($this->meta,$this->columns);
		$config = array('reader'=>$reader);
		$config['url'] = $this->initialed = $this->url;

		$this->store = new SOSO_Base_Data_Store($config);
		$connection = $this->store->getProxy();
		$connection->on('beforerequest',function($file,$option,$conn){
			$conn->cache = isset($conn->cache) ? $conn->cache : array();
			if (isset($conn->cache[$file])) {
				$conn->mContent = $conn->cache[$file];
				return false;
			}else{
				$cacher = new SOSO_Cache_File(array('cache_dir'=>SOSO_Frameworks_Config::getSystemPath('temp')));
				$cacher->setOption('use_encode',false);
				$key = $cacher->getKey($file);
				$content = $cacher->read($key);
				if (!$content) {
					return true;
				}
				
				$conn->mContent = $conn->cache[$file] = $content;
				return false;
			}
		});

		$connection->on('afterrequest',function($file,$option,$conn){
			$conn->cache = isset($conn->cache) ? $conn->cache : array();
			$conn->cache[$file] = $conn->mCache;
			$cacher = new SOSO_Cache_File(array('cache_dir'=>SOSO_Frameworks_Config::getSystemPath('temp')));
			$cacher->setOption('use_encode',false);
			$key = $cacher->getKey($file);
			if(!$cacher->isCached($key)) $cacher->write($key,$conn->mContent);
		});
	}
	public function setUrl($url){
		$this->url = $url;
		$this->init($this->meta, $this->columns);
		$this->store->setUrl($url);
		return $this;
	}

	public function reconfig($meta,$columns=null){
		if (is_array($meta)){
			if (isset($meta['meta'])) $this->meta = $meta['meta'];
			if (isset($meta['columns'])) $this->columns = $meta['columns'];
		}else{
			$this->meta = $meta;
			$this->columns = $columns;
		}
		
		if($this->reader) $this->reader->reconfigure($this->meta,$this->columns);
		return !!$this->reader;
	}
}
