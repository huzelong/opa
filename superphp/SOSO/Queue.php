<?php
/**
 * 
 * @author zhangyong
 * 偷懒的队列类
 * 
 */
class SOSO_Queue extends SOSO_Base_Util_Observer{
	protected $mTube = 'default';
	protected $mJob;
	
	/**
	 * @throws Exception
	 */
	public function __construct($config=array()){
		$tube = 'default';
		$tConnectionTimeout = null;
		if(!$config){
			$config = SOSO_Frameworks_Config::getConfigParam('taskqueue');
			if(!$config){
				$msg = "You must config taskqueue server and tube in web.xml like : \n ";
				$msg.= " <params><param name=\"taskqueue\" value=\"YOUR_HOST:PORT/YOUR_TUBE\" /></params>";
				if(php_sapi_name() !== 'cli'){
					$msg = htmlspecialchars($msg);
				}
				throw new Exception($msg);
			}
			$arr = explode("/",$config);
			$server = $arr[0];
			if(isset($arr[1])){
				$tube = $arr[1];
			}
		}else{
			$server = $config['server'];
			if(isset($config['tube'])) {
				$tube = $config['tube'];
			}
			if(isset($config['conntectionTimeout'])){
				$tConnectionTimeout = $config['conntectionTimeout'];
			}
		}
		
		$port = Pheanstalk_PheanstalkInterface::DEFAULT_PORT;
		if(false !== strpos($server,':')){
			list($server,$port) = explode(":",$server); 
		}
		parent::__construct(new Pheanstalk_Pheanstalk($server,$port,$tConnectionTimeout));
		$this->setTube($tube);
	}
	
	public function setTube($pTube,$pOnly=false){
		$this->mTube = $pTube;
		$this->useTube($pTube);
		if($pOnly){
			$this->watchOnly($pTube);
		}else{
			$this->watch($pTube);
		}
		return $this;
	}
	
	public function getTube(){
		return $this->mTube;
	}
	
	public function getJob($timeout=null){
		return $this->reserve($timeout);
	}
	
	public function del($job){
		if($job)
			$this->delete($job);
		return $this;
	}
	
	/**
	 * Puts a job into a 'buried' state, revived only by 'kick' command.
	 *
	 * @param Pheanstalk_Job $job
	 * @return void
	 */
	public function bury($job){
		return $this->proxy->bury($job);
	}
	
	/**
	 * Puts a job on the queue.
	 *
	 * @param string $data The job data
	 * @param int $priority From 0 (most urgent) to 0xFFFFFFFF (least urgent)
	 * @param int $delay Seconds to wait before job becomes ready
	 * @param int $ttr Time To Run: seconds a job can be reserved for
	 * @return int The new job ID
	 */
	public function addTask($data, $priority = 1024, $delay = 0, $ttr = 60){
		return $this->put($data,$priority,$delay,$ttr);
	}	
	
	/**
	 * @throws Exception
	 * @return boolean
	 */
	public function getTaskData($timeout){
		$this->mJob = $this->getJob($timeout);
		if(!$this->mJob) return false;
		
		return $this->mJob->getData();
	}
	
	public function deleteJob(){
		$this->del($this->mJob);
		return $this;
	}
}

?>
