<?php
/**
 * 
 * @author moonzhang
 * Another parser for (X|HT)ML(stream version)
 * @see SOSO/Base/Data/XMLReader & SOSO/Base/Util/XMLQuery
 * @version 1.0 2013-05-25 13:33
 */
class SOSO_Util_XMLParser extends XMLReader {
	const STATE_OPEN  = 1;
	const STATE_CLOSE = 0;

	protected $mStop = false;
	protected $mAutoRead = true;

	protected $_verbos = false,
		$_stack=array(),
		$_rawdata,
		$_tagName,
		/*$_stream,*/
		$_state;
	
	public function __construct($verbos=false){
		$this->_verbos = !!$verbos;
	}
	
	public function setVerbos($flag=true){
		$this->_verbos = $flag;
		return $this;
	}
	
	public function setAutoRead($flag=true){
		$this->mAutoRead = !!$flag;
		return $this;
	}
	
	public function reset(){
		$this->_rawdata = '';
		$this->_stack = array();
	}
	
	public static function load($stream,$encoding="utf-8",$options=array()){
		$reader = new self();
		$reader->open($stream,$encoding,$options);
		return $reader;
	}
	
	public function feed($data,$encoding="utf-8",$options=null){
		$this->_rawdata = $this->_rawdata . $data;
	 	$this->XML($this->_rawdata,$encoding,$options);
	 	$this->yamiedie();
	}
	
	public function open($URI,$encoding=null,$options=null){
		parent::open($URI,$encoding,$options);
		if($this->mAutoRead) $this->yamiedie();
	}
	
	protected function verbos($msg){
		if(!$this->_verbos) return;
		$msg = sprintf("%s%s\n",str_repeat("\t",$this->getLevel()),$msg);
		//$this->_stack[] = $this->name;
		//echo $msg;
		fwrite(STDERR,$msg);
	}
	
	protected function finish_shorttag($tag,$attr=array()){
		$this->finish_starttag($tag,$attr);
		$this->finish_endtag($tag);
	}
	
	protected function finish_endtag($tag){
		$msg = sprintf("%s entity end\n",$tag);
		$this->verbos($msg);
		
		$method = 'end_'.$tag;
		if(method_exists($this, $method)){
			$this->$method($tag);
		}else{
			$this->handle_endtag($tag);
		}
		//array_push($this->_stack,'/'.$tag);
		array_pop($this->_stack);
	}
	
	protected function finish_starttag($tag,$attr=array()){
		$msg = sprintf("%s element start",$tag);
		$this->verbos($msg);
		
		array_push($this->_stack,$tag);
		$methods = array('start_'.$tag,'do_'.$tag);
		foreach($methods as $method){
			if(method_exists($this, $method)){
				return $this->$method($attr);
			}	
		}
		
		return $this->handle_starttag($tag,$attr);
	}
	
	final protected function go(){
		$this->_tagName = $tag = $this->name;
			
		if($this->nodeType == XMLReader::END_ELEMENT){
			$this->finish_endtag($this->name);
		}
		
		if($this->nodeType == XMLReader::TEXT){
			$msg = sprintf("data:%s\n",$this->value);
			$this->verbos($msg);
			$this->handle_data($this->value);
		}elseif($this->nodeType == XMLReader::CDATA){
			$msg = sprintf("data:%s\n",$this->value);
			$this->verbos($msg);
			$this->handle_data($this->value);
		}elseif($this->nodeType == XMLReader::ELEMENT){
			if($this->isEmptyElement){
				$this->finish_shorttag($this->name,$this->getAttr());
				return;
			}
		
			$this->finish_starttag($this->name,$this->getAttr());
		}
	}

	public function pause(){
		$this->mStop = true;
		return $this;
	}

	public function resume(){
		$this->mStop = false;
		return $this;
	}

	/**
	 * main
	 */
	public function yamiedie(){
		while(!$this->mStop && $this->read()){
			$this->go();
		}
	}
	
	protected function getTag(){
		return $this->_tagName;
	}
	
	protected function getAttr(){
		$arr = array();
		if($this->hasAttributes){
			$arr = array();
			while($this->moveToNextAttribute()) {
				$arr[$this->name] = $this->value;
			}
		}
		$this->moveToElement();
		return $arr;
	}
	
	public function getStack(){
		return array()+$this->_stack;
	}
	
	protected function getLevel(){
		return $this->depth;
		//return $this->_level;
	}
	
	protected function getDom($type="dom"){
		$dom = new DOMDocument('1.0', 'utf-8');
		$d = $dom->importNode($this->expand(),true);
		$dom->appendChild($d);
		return 'dom'==$type ? $d : simplexml_import_dom($d);
	}
	
	/**
	 * 将XML结构转换成数组结构.
	 *
	 * @param SimpleXMLElement|DOMDocument|DOMNode $obj SimpleXMLElement, DOMDocument or DOMNode instance
	 * @return array Array representation of the XML structure.
	 */
	public static function domToArray($obj) {
		if ($obj instanceof DOMNode) {
			$obj = simplexml_import_dom($obj);
		}
	
		$result = array();
		$namespaces = array_merge(array('' => ''), $obj->getNamespaces(true));
	
		self::_toArray($obj, $result, '', array_keys($namespaces));
		return $result;
	}
	
	protected static function _toArray($xml, &$parentData, $ns, $namespaces) {
		$data = array();
	
		foreach ($namespaces as $namespace) {
			foreach ($xml->attributes($namespace, true) as $key => $value) {
				if (!empty($namespace)) {
					$key = $namespace . ':' . $key;
				}
				$data['@' . $key] = (string)$value;
			}
	
			foreach ($xml->children($namespace, true) as $child) {
				self::_toArray($child, $data, $namespace, $namespaces);
			}
		}
	
		$asString = trim((string)$xml);
		if (empty($data)) {
			$data = $asString;
		} elseif (!empty($asString)) {
			$data['@'] = $asString;
		}
	
		if (!empty($ns)) {
			$ns .= ':';
		}
		$name = $ns . $xml->getName();
		if (isset($parentData[$name])) {
			if (!is_array($parentData[$name]) || !isset($parentData[$name][0])) {
				$parentData[$name] = array($parentData[$name]);
			}
			$parentData[$name][] = $data;
		} else {
			$parentData[$name] = $data;
		}
	}
	
	/** ##### 以下函数可能需要被重载以便能够处理数据，如果有(start|end)_XXXtag 可以不处理 ####*/
	
	/**
	 * 空函数，可以被重载
	 * @param unknown_type $tag
	 * @param unknown_type $attr
	 */
	protected function handle_starttag($tag,$attr=array()){
	
	}
	/**
	 * 空函数，可以被重载
	 * @param unknown_type $tag
	 */
	protected function handle_endtag($tag){
	
	}
	
	protected function handle_data($data){
	
	}
}
