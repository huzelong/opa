<?php
/**
 * SOSO Framework
 * 
 * @category   SOSO_Util
 * @package    SOSO_Util
 * @description 工具类::分页程序
 * @copyright  Copyright (c) 2007-2008 Soso.com
 * @version 1.0 alpha 2006-03-02 moonzhang@sogou-inc.com
 * 
 * @todo 编码自适应
 * @todo 重构?
 */
class SOSO_Util_Pagination /*extends SOSO_Base_Util_Observable*/ implements SOSO_Interface_PagerFormatter {
	
	/**
	 * 总页数
	 * @var integer
	 */
	protected $mTotal;        
	
	/**
	 * 当前页数
	 * @var integer
	 */
	protected $mCurrentPage = 1;  
	
	/**
	 * 前一页
	 * @var integer
	 */
	protected $mPrePage ;    
	
	/**
	 * 下一页
	 * @var integer
	 */
	protected $mNextPage;
	
	/**
	 * 最多显示数字个数 
	 * @var integer
	 */
	protected $mLength = 9;
	
	/**
	 * 是否显示 [最前页],[最后页] 开关,true为显示
	 * @var boolean
	 */
	protected $showFirst = true;

	/**
	 * 参数名称
	 *
	 * @var string
	 */
	protected $mParam = 'page';
	/**
	 * 记录总数
	 * @var integar
	 */
	protected $mTotalRecords = 0;
	
	/**
	 * 默认链接
	 */
	protected $mLink = "<a href='%s'>%s</a>&nbsp;";
	
	/**
	 * 生成的分页链接
	 *
	 * @var unknown_type
	 */
	protected $mPage;
	protected $mPagesize ;
	/**
	 * 
	 * @var SOSO_Interface_PagerFormatter
	 */
	protected $mFormatter;
	protected $mEncoding = 'gbk';
	protected $mDefaultEncoding = 'gbk';
	/**
	 * 构造函数，初始化参数
	 *
	 * @param integer $pPageNo     //当前页码
	 * @param integer $pPageSize   //页尺寸
	 * @param integer $total的     //总记录数
	 * @param string  $psType      //分页样式
	 */
	public function __construct($pPageNo=1,$pPageSize=30,$total,SOSO_Interface_PagerFormatter $fmt=null,$pEncoding='gbk',$pParamName='page'){
		$this->setParameter($pParamName);
		
		/*if (isset($_GET[$this->mParam])){
			$pPageNo = intval($_GET[$this->mParam]);
		}*/
		
		$this->reInit($pPageNo,$pPageSize,$total);
		$this->setFormatter($fmt);
		if (strlen($pEncoding)){
			$this->setEncoding($pEncoding);
		}
	}
	
	public function setFormatter(SOSO_Interface_PagerFormatter $fmt=null){
		if(is_null($fmt)) $fmt = $this;
		$this->mFormatter = $fmt;
		return $this;
	}
	
	public function setEncoding($encoding){
		$this->mEncoding = $encoding;
		return $this;
	}
	
	public function getEncoding(){
		return $this->mEncoding;
	}
	
	public function setDefaultEncoding($encoding){
		$this->mDefaultEncoding = $encoding;
		return $this;
	}
	
	public function getDefaultEncoding(){
		return $this->mDefaultEncoding;
	}
	
	public function setParameter($param=''){
		$this->mParam = strlen($param) ? $param : 'page';
		return $this;
	}
	
	protected function init(){
		$this->mTotal       = ($this->mTotalRecords)?ceil($this->mTotalRecords/$this->mPagesize):0;
		$this->mNextPage    = ($this->mCurrentPage+1 > $this->mTotal)?$this->mTotal:$this->mCurrentPage+1;
		$this->mPrePage     = ($this->mCurrentPage-1>0)? $this->mCurrentPage-1 : 1;
	}
	
	public function reInit($curr,$pagesize,$totalRecords=0){
		$this->setCurrentPage($curr);
		$this->setPagesize($pagesize);
		$this->setTotalRecords($totalRecords);
		$this->init();
		return $this;
	}

	protected function setTotalRecords($num){
		$this->mTotalRecords = $num;
		return $this;
	}
	
	public function getTotalRecords(){
		return $this->mTotalRecords;
	}
	
	public function setCurrentPage($num){
		$this->mCurrentPage = intval($num);
		$this->mCurrentPage <=0 && $this->mCurrentPage = 1;
		return $this;
	}
	
	public function setPagerData($data){
		$this->mPage = $data;
		return $this;	
	}
	
	/*public function getPagerString(){
		return $this->mPage;
	}*/
	
	public function getTotalPage(){
		return $this->mTotal;
	}
	
	public function getCurrentPage(){
		return $this->mCurrentPage;
	}
	
	public function getNextPage(){
		return $this->mNextPage;
	}
	
	public function getPrePage(){
		return $this->mPrePage;
	}
	
	public function getPagesize(){
		return $this->mPagesize;
	}
	
	public function setPagerLength($len){
		$this->mLength = $len;
		return $this;
	}
	
	public function getPagerLength(){
		return $this->mLength;
	}
	
	public function setPagesize($size){
		$this->mPagesize = intval($size);
		return $this;
	}
	
	/**
	  * 生成分页效果
	  * 
	  */
	public function getPage(){
		$this->mFormatter->format($this);
		if(is_string($this->mPage) && $this->mDefaultEncoding != $this->mEncoding){
			return iconv($this->mDefaultEncoding,$this->mEncoding,$this->mPage);
		}
		return $this->mPage;
	}

	
	public function format(SOSO_Util_Pagination $pagination){
		if ($this->getTotalPage() === 1){
			$str = "共 1 页";
			$this->setPagerData($str);
			return $str;
		}
			
		if ($this->mTotal == 0)
			return false;
		$startNum = 1;
		if ($startNum + floor ( $this->mLength / 2 ) < $this->mCurrentPage) {
			$startNum = $this->mCurrentPage - floor ( $this->mLength / 2 ) - 1;
		}
		if ($startNum + $this->mLength <= $this->mTotal) {
			$endNum = $startNum + $this->mLength;
		} else {
			$endNum = $this->mTotal;
			// $startNum = $this->mTotal - $this->mLength;
		}
		$first = '';
		$end = '';
		$main = '';
		$prefix = '?';
		if (isset ( $_GET [$this->mParam] )) {
			$_GET [$this->mParam] = '';
			unset ( $_GET [$this->mParam] );
		}
		$prefix .= empty ( $_GET ) ? '' : http_build_query ( $_GET ) . '&';
		$link = "{$this->mParam}=%d";
		if ($this->showFirst) {
			$first = sprintf ( $this->mLink, $prefix . sprintf ( $link, 1 ), "最前页" );
			$end = sprintf ( $this->mLink, $prefix . sprintf ( $link, $this->mTotal ), "最后页" );
		}
		for($i = $startNum; $i <= $endNum; $i ++) {
			$link = $prefix . $this->mParam . "=$i";
			$main .= $this->mCurrentPage == $i ? '[' . $i . ']&nbsp;' : "<a href='$link'>$i</a>&nbsp;";
		}
		$tPagerString = $first . $main . $end;
		$pagination->setPagerData($tPagerString);
		return $tPagerString;
	}
}
?>
