<?php
class SOSO_Auth_Oss extends SOSO_Base_Util_Observable implements SOSO_Auth_Interface,SOSO_Interface_Runnable {
	const LOGIN_URL = 'http://login.d.sogou-inc.com/?url=';
	
	private $mBackurl = '';

	public function getIdentify($key='currentUser'){
		return $this->getName($key);
	}

	/**
    * 返回当前请求用户的名称
    * @return   void
    */
	public function getName($key='currentUser'){
		return isset($_SESSION[$key]) ? $_SESSION[$key] : isset($_SESSION['currentUser']) ? $_SESSION['currentUser'] : null;
	}

	/**
    * 检查当前请求的用户是否是合法用户
    * @return   void
    */
	public function isAuthorized($key='currentUser'){
		return isset($_SESSION[$key]) && is_object($_SESSION[$key]) && isset($_SESSION[$key]->mUserID);
	}

	/**
    * 进行用户认证,认证成功返回true，否则返回false
    * @param    string $pUserName
    * @param    string $pPassword
    * @return   boolean
    */
	public function login($pUserName, $pPassword, $pBackUrl="/",$session_key='currentUser'){
		$tHost = "http://".$_SERVER['HTTP_HOST'];
		$this->mBackurl = str_ireplace($tHost,"",$pBackUrl);

		$tBackUrl = $tHost."/".__CLASS__.".php?session_key=".$session_key."&backUrl=".base64_encode($this->mBackurl);
		SOSO_Util_Util::redirect(self::LOGIN_URL . rawurlencode($tBackUrl));
		exit;
	}

	public function run(){
		$backUrl = isset($_GET['backUrl']) ? base64_decode($_GET['backUrl']) : '/';
		$key = isset($_GET['session_key']) ? $_GET['session_key'] : 'currentUser';

		if (isset($_GET['ticket'])) {
			$ret = $this->parseTicket($_GET['ticket'],$backUrl,$key);
			//todo 完成这个函数
			exit;
		}else{
			$this->login('','',base64_decode($backUrl));
		}

		if(!$ret) return;
		if(isset($_SESSION[$key])){
			$_SESSION[$key]->mUserID = $ret['StaffId'];
		}else{
			$_SESSION[$key] = (object)array('LoginName'=>$ret[$key],'mUserID'=>$ret['StaffId']);
		}
		SOSO_Session::writeClose();
		SOSO_Util_Util::redirect($backUrl);
	}

	protected function parseTicket($ticket,$backUrl='/',$session_key='username'){
		$ticket = base64_decode($ticket);
		$oSign = SOSO_Util_Signature::getInstance();
		$str = $oSign->decode($ticket);
		
		if(!$str){
			$this->login('','',base64_decode($backUrl));
			return;
		}
		parse_str($str,$arr);
		return array_merge($arr,array($session_key=>$arr['username']));
	}

	/**
    * 登录退出
    * @return   boolean
    */
	public function logout($pBackUrl=null){
		
		if (stripos($pBackUrl,'http') === false) {
			$pBackUrl = preg_replace("#/+#i",'/',$pBackUrl);
			/*if (substr($pBackUrl,0,1) == '/') {
			$pBackUrl = substr($pBackUrl,1);
			}*/
			$tHost = "http://".$_SERVER['HTTP_HOST'];
			$pBackUrl = $tHost."/".get_class($this).".php?backUrl=".base64_encode($pBackUrl);
		}
		SOSO_Util_Util::redirect("http://passport.oa.com/modules/passport/signout.ashx?url=".rawurlencode($pBackUrl));
	}
}
