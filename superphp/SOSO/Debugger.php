<?php
/**
 * Debugger
 * 
 * @author moonzhang
 * 
 * example : SOSO_Debugger::instance()->setLogger(new SOSO_Logger_Debugger());
 * config : mode="deubug" | mode="remote_debug" allow_ip="ip1[,ip2]"
 */

class SOSO_Debugger {
	
	protected static $s;
	private $data = array();
	private $logger;
	const DEBUGGER_NAME = 'DEBUGGER';
	
	private function __construct(){
	}
	
	public function setLogger(SOSO_Logger_Abstract $logger=null){
		if($logger) {
			$this->logger = $logger;
		}else{
			$this->logger = new SOSO_Logger_Debugger();
		}
		SOSO_Log::getLogging(self::DEBUGGER_NAME)->addLogger($this->logger);
		$this->logger->setBuffering(true)->setBufferSize(1024);
		return $this;
	}
	
	public static function instance(){
		if (is_null(self::$s)) self::$s = new self(); 
		return self::$s;
	}
	
	public function __destruct(){
		if(!SOSO_Frameworks_Config::isDebugMode()){
			return false;
		}
		
		if(!$this->logger){
			$cmdline = 'cli' === php_sapi_name();
			if($cmdline){
				echo "\n\n";
				$line = str_repeat("--", 20);
				echo sprintf("%s Debug Info %s\n",$line,$line);
			}
		}
		$this->export();
	}
	
	public function log($line,$type="Custom",$context=array()){		
		if(!$this->canDebug()){
			return false;
		}
		
		if($this->logger){
			SOSO_Log::getLogging(self::DEBUGGER_NAME)->info(array('type'=>$type,'info'=>$line),$context);
			return $this;
		}
		
		if(!isset($this->data[$type])) $this->data[$type] = array();
		array_push($this->data[$type],$line);
		return $this;
	}
	
	public function export(){
		if(!$this->data){
			return false;
		}
		if($this->logger){
			$this->logger->debug($this->data);
			return true;
		}
		
		$isAJAX = SOSO_Util_Util::isAJAXRequest();
		if($isAJAX){
			echo "\n/*";
		}else{
			echo "\n<pre>";
		}
		
		print_r($this->data);
		
		if($isAJAX){
			echo "*/";
		}else{
			echo "\n</pre>";
		}
	}
	
	public function canDebug(){
		if($this->logger){
			return $this->logger->canDebug();
		}
		
		return SOSO_Frameworks_Config::canDebug();
	}
}
