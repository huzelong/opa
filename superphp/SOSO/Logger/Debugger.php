<?php
/**
 * 
 * @author zhangyong
 * @version 1.0 
 * 供debugger使用
 */
class SOSO_Logger_Debugger extends SOSO_Logger_Browser implements SOSO_Interface_Debuggable{
	private $date_fmt = 'H:i:s';
	
	public function canDebug(){
		return SOSO_Frameworks_Config::canDebug();
	}
	
	protected function flush(){
		
		if(!$this->buffer) return;
		
		$cmdline = 'cli' === php_sapi_name();
		if($cmdline){
			echo "\n\n";
			$line = str_repeat("--", 20);
			echo sprintf("%s Debug Info %s\n",$line,$line);
		}
		$logs = array();
		
		foreach ($this->buffer as $record){
			$data = json_decode($record->getMessage(),true);
			$type = $data['type'];
			if(!isset($logs[$type])){
				$logs[$type] = array();
			}
			unset($data['type']);
			$tmp = array('time'=>$record->getDatetime()->format($this->date_fmt));
			if($record->getContext()){
				$tmp['context'] = json_encode($record->getContext());
			}
			$logs[$type][] = array_merge($data,$tmp);
		}
	 
		$isAJAX = SOSO_Util_Util::isAJAXRequest();
		$cmdline = 'cli' === php_sapi_name();
		echo "\n\n";
		
		if(!$cmdline){
			if(!$isAJAX){
				echo "<script>";
				echo "
				if(typeof console.table == 'undefined'){
					console.table = function(a){
						if(Object.prototype.toString.call(a) === '[object Array]'){
						  for(var i=0;i<a.length;i++){
						     console.log(i + \"\t\" + a[i].info + \"\t\" + a[i].time);
						  }
						}else{
							console.log(i + \"\t\" + a.info + \"\t\" + a.time);
						}
					}
				}";
			}else{
				echo "/*";
			}
		}
		
		foreach ($logs as $group=>$arr){
			$this->group($group,$arr);
		}
		
		if(!$cmdline){
			if(!$isAJAX){
				echo "</script>";
			}else{
				echo "*/";
			}
		}
		$this->buffer = array();
	}
	
	protected function group($group,$arr){
		if('cli' === php_sapi_name()){
			echo "\n";
			//echo str_repeat(" -- ", 15);
			echo "\n  >> $group\n";
			foreach ($arr as $k=>$info){
				printf("\t%d\t%s\t%s\n",$k,$info['info'],$info['time']);
			}
			echo "\n\n";
			return;
		}
		
		if(stripos($group,"error") === false && stripos($group,"exception") === false){
			echo "\nconsole.group('$group');\n";
			$this->table(json_encode($arr));
		}else{
			echo "\nconsole.groupCollapsed('$group');\n";
			foreach ($arr as $d){
				//echo sprintf("\n\tconsole.warn(\"%s\");\n",preg_replace('#(["\'\r\n])#','\\\\$1',$d['info']));
				
				if(stripos($group,"error") === false){
					//print_r($d);
					echo sprintf("\n\tconsole.warn(\"%s\",%s);\n",preg_replace('#(["\'\r\n])#','\\\\$1',$d['info']),$d['context']);
				}else 
					echo sprintf("\n\tconsole.warn(\"%s\");\n",preg_replace('#(["\'\r\n])#','\\\\$1',$d['info']));
				
			}
		}
		
		echo "console.groupEnd('$group');";
	} 
	
	protected function table($data){
		echo "console.table($data);";
	}
}

?>
