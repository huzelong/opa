<?php
/**
 * @author moonzhang
 * @verion 1.0 2012-07-05
 * 
 * 用来向一组文件写日志：使用指定文件大小来控制
 */
require_once "Rotating.php";

class SOSO_Logger_XFileRotating extends SOSO_Logger_Rotating{
	protected $maxFilesize = 0;
	protected $fileCount = 0;
	
	protected function log(SOSO_Logger_Message $message){
		if ($this->canRotate($message)){
			$this->doRotate();
		}
		file_put_contents($this->url, (string) $message->getFormatted(),FILE_APPENDFILE_APPEND | LOCK_EX);
	}
	
	public function setFilecount($num){
		$this->fileCount = (int)$num;
		return $this;
	}
	
	public function setFilesize($bytes){
		$this->maxFilesize = (int)$bytes;
		return $this;
	}
	
	public function canRotate(SOSO_Logger_Message $message){
		if ($this->maxFilesize > 0){
			$msgString = $message->getFormatted();
			fseek($this->stream, 0,2);
			return ftell($this->stream) + strlen($msgString) >= $this->maxFilesize;
		}
		return false;
	}	
	
	public function doRotate(){
		$stat = fstat($this->stream);
		if ($this->stream) $this->close();
		$fmt = "%s.%d";
		if ($this->fileCount > 0){
			foreach (range($this->fileCount-1,0,-1) as $num){
				$fromFile = sprintf($fmt,$this->url,$num);
				$toFile = sprintf($fmt,$this->url,$num + 1);
				if (file_exists($fromFile)){
					if (file_exists($toFile) && is_writable($toFile)){
						unlink($toFile);
					}
					rename($fromFile, $toFile);
				}
			}
			$toFile = $this->url . '.1';
			if (file_exists($toFile)) unlink($toFile);
			rename($this->url,$toFile);
		}else{
			$fromFile = $this->url;
			$toFile = sprintf($fmt,$this->url,date("YmdHis"));
			
			$lock = sprintf("%s/%s_%s.lock", dirname($this->url),basename($this->url),$stat['ino']);
			if(mkdir($lock)){
				rename($fromFile, $toFile);

			}	
		}
		//lock + w-mode?
		//if (file_exists($this->url)) unlink($this->url);
	}
} 
