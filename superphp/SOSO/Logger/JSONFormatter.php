<?php
class SOSO_Logger_JSONFormatter implements SOSO_Logger_IFormatter{

    const DATE = "Y-m-d H:i:s";
    protected $dateFormat = '';

    public function __construct($format = null, $dateFormat = null){
        $this->setDateFormat($dateFormat);
    }

    public function setDateFormat($dateFormat){
        $this->dateFormat = $dateFormat ? $dateFormat : self::DATE;
        return $this;
    }
 	public function format(SOSO_Logger_Message $message){
        $vars = $message->getArrayCopy();
        $vars['datetime'] = $message->getDatetime()->format($this->dateFormat);

 		if(defined("JSON_UNESCAPED_UNICODE")){
 			return json_encode($vars,JSON_UNESCAPED_UNICODE);
 		}
        return json_encode($vars);
    }
    
    public function setFormat($format){
    	return $this;
    }
}
