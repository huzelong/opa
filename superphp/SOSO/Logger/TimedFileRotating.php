<?php
/**
 * @author moonzhang
 * @verion 1.0 2012-07-05
 * 
 * 用来向一组文件写日志：以一定的时间周期
 */
require_once "Rotating.php";

class SOSO_Logger_TimedFileRotating extends SOSO_Logger_Rotating{
	protected $when;
	//protected $fileCount = 0;
	protected $rotateTime;
	protected $interval;
	protected $format;
	protected $pattern;
	protected $dayOfWeek = 0;
	protected $filename;
	private   $fileParts = array();
	protected $autuHash = false;
	protected $hashLevel = 2;
	protected $baseDir;
	protected $workDir;
	
	public function __construct($filename, $when='h', $interval=1 ,$level = SOSO_Log::DEBUG, $bubble = true,$buffering=false){
		parent::__construct($filename,$level,$bubble,$buffering);
		//$this->setFilecount($fileCount);
        $this->when = strtoupper($when);

        $config = array(
        	/*秒*/
        	'S'=>array(1,"%Y-%m-%d-%H-%M-%S","#^\d{4}-\d{2}-\d{2}_\d{2}-\d{2}-\d{2}$#"),
        	/*分*/
        	'I'=>array(60,'%Y-%m-%d-%H-%M','#^\d{4}-\d{2}-\d{2}_\d{2}-\d{2}$#'),
        	/*时*/
        	'H'=>array(60*60,'%Y-%m-%d-%H','#^\d{4}-\d{2}-\d{2}_\d{2}$#'),
        	/*天*/
        	'D'=>array(60*60*24,'%Y-%m-%d','#^\d{4}-\d{2}-\d{2}$#'),
        	/* 周 */
        	'W'=>array(60*60*24*7,'%Y-%m-%W','#^\d{4}-\d{2}-\d{2}$#'),
			/** 月 */
			'M'=>array(60*60*24*30,'%Y-%m','#^\d{4}-\d{2}$#'),
        );

        $when = substr($this->when,0,1);
        if (!isset($config[$when])){
        	throw new Exception("What're you doing??",250);
        }
        
        $data = $config[$when];
       // if ($when == 'W') $this->dayOfWeek = int(substr($this->when,1));
		$this->when = $when;
        //$this->interval = $data[0] * $interval;
		$this->interval = $interval;
        $this->format = $data[1];
        $this->pattern = $data[2];
		preg_match("#^(.*)(\.[a-z]+)$#si",$filename,$match);
		$this->fileParts = array($match[1],$match[2]);

		if($interval > 1){
			$parts = explode("-",$data[1]);
			array_pop($parts);
			array_push($parts,'');
			$this->format = join("-",$parts);
		}

		$this->baseDir = $this->workDir = dirname($filename);
		umask(000);
		@mkdir($this->baseDir,0777,true);


		//$this->getTimedFilename(new Datetime());
		/*
        if(file_exists($this->url)){
        	$time = filemtime($this->url);
        }else{
        	$time = time();
        }
        $this->rotateTime = $this->computeRotateTime($time);*/
	}

	protected function getTimedFilename(Datetime $datetime){
		$sep = strftime('-' . $this->format,$datetime->getTimestamp());
		if($this->interval > 1){
			$format = 's';
			switch ($this->when){
				case 'M': $format = 'm';break;
				case 'W': $format = 'w';break;
				case 'D': $format = 'd';break;
				case 'H': $format = 'h';break;
				case 'I': $format = 'i';break;
			}
			$sep .= floor($datetime->format($format) / $this->interval);
		}
		return join($sep,$this->fileParts);
	}

	public function setHashLevel($level){
		if($level > 0){
			$this->hashLevel = $level;
		}

		return $this;
	}

	public function enableHash(){
		$this->autuHash = true;
		$this->hash();
		return $this;
	}

	public function setBaseDir($path){
		$this->baseDir = $path;
		$this->fileParts[0] = $this->baseDir . DIRECTORY_SEPARATOR . basename($this->fileParts[0]);
		return $this;
	}

	public function getBaseDir(){
		return $this->baseDir;
	}

	public function disableHash(){
		$this->autuHash = false;
		return $this;
	}

	public function hash() {
		$pieces = explode('-',$this->format);
		$format = join(DIRECTORY_SEPARATOR,array_splice($pieces,0,$this->hashLevel)).DIRECTORY_SEPARATOR;
		$path = strftime($format);
		$target = $this->baseDir . DIRECTORY_SEPARATOR . $path;
		if($target === $this->workDir) return $this;
		$this->fileParts[0] = $target . DIRECTORY_SEPARATOR . basename($this->fileParts[0]);
		umask(000);
		@mkdir($target,0777,true);
		$this->workDir = $target;
		return $this;
	}

	/**
	 * 
	 * 根据指定时间计算时间
	 * @param unknown_type $currentTime
	 */
	public function computeRotateTime($currentTime){
		$result = $currentTime + $this->interval;
		return $result ;
	}

	/*public function setFilecount($num){
		$this->fileCount = (int)$num;
		return $this;
	}*/
	
	public function canRotate(SOSO_Logger_Message $message){
		/*$time = time();
        return $time >= $this->rotateTime;*/

		if($this->autuHash){
			$this->hash();
		}

		$filename = $this->getTimedFilename($message->getDatetime());
		$rv = ($this->filename && $this->filename != $filename);

		$this->filename = $this->url = $filename;

		return $rv;
	}	
	
	public function doRotate(){
		if ($this->stream) $this->close();
		$this->url = $this->filename;
		return;

		//old revision
		if ($this->stream) $this->close();
		$time = $this->rotateTime - $this->interval;
		$toFile = $this->url . '.' . strftime($this->format,$time);
		if (file_exists($toFile)) unlink($toFile);
		file_exists($this->url) && rename($this->url, $toFile);
		
		/*if ($this->fileCount > 0){
			//todo 删除文件逻辑
		}*/
		
		$currentTime = time();
		$newRotateTime = $this->computeRotateTime($currentTime);
		while($newRotateTime <= $currentTime){
			$newRotateTime += $this->interval;
		}
		$this->rotateTime = $newRotateTime;
	}
} 
